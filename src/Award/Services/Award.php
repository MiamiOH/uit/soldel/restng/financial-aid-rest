<?php

/*
	-----------------------------------------------------------
	FILE NAME: Award.class.php

	Copyright (c) 2016 Miami University, All Rights Reserved.

	Miami University grants you ("Licensee") a non-exclusive, royalty free,
	license to use, modify and redistribute this software in source and
	binary code form, provided that i) this copyright notice and license
	appear on all copies of the software; and ii) Licensee does not utilize
	the software in a manner which is disparaging to Miami University.

	This software is provided "AS IS" and any express or implied warranties,
	including, but not limited to, the implied warranties of merchantability
	and fitness for a particular purpose are disclaimed. It has been tested
	and is believed to work as intended within Miami University's
	environment. Miami University does not warrant this software to work as
	designed in any other environment.

	AUTHOR: Erin Mills

	DESCRIPTION:  The Award service is currently designed for GET only

	INPUT:
	PARAMETERS: pidm, uniqueId or bannerId

	ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

	TABLE USAGE:

	AUDIT TRAIL:

	DATE    PRJ-TSK          UniqueID
	Description:

	03/2016			MILLSE
	Description:	Initial Program

	04/04/2016		MILLSE/PATELAH
	Description:	Updates for security/passing unit tests
 */

namespace MiamiOH\FinancialAidRest\Award\Services;

class Award extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD'; // secure datasource

    /*
    * Helper function to Set the Data Source to be used called by the frame work
    *
    * Inputs:
    * datasource: Name of Data Source to use.
    */
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    /*
    * Helper function to Set the Database source to be Used
    *
    * Inputs:
    * database: Name of database source to use.
    */
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    /*
    * Helper function to Set the Configuration to be Used
    *
    * Inputs:
    * configuration: Name of Configuration source to use.
    */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /*
     * Get Award Information (GET)
     *
     * Parameters (URL):
     * token: Authentication Token (WebServices/StudentFinancialAid view Access Required)
     * pidms: PIDMs and/or Uniqueids whose awards are to be to Retrived
     *
     * Return: Returns an array of all awards of the users requested.  Does not check for
     * 			duplicates across the pidm/uniqueid lists. This way the user gets back
     * 			the users they request. No matter what.
     */
    public function getAward()
    {
        //log
        $this->log->debug('Start the award service.');

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();
        $pidms = array();
        $uniqueids = array();
        $bannerids = array();
        $fundTypeCode = '';

        //check to be sure a pidm or uniqueid or bannerid was added
        if (!$options ||
            ((!isset($options['pidm']) || !is_array($options['pidm']) || count($options['pidm']) == 0 || (count($options['pidm']) == 1 && $options['pidm'][0] == ""))
                && (!isset($options['uniqueid']) || !is_array($options['uniqueid']) || count($options['uniqueid']) == 0 || (count($options['uniqueid']) == 1 && $options['uniqueid'][0] == ""))
                && (!isset($options['bannerid']) || !is_array($options['bannerid']) || count($options['bannerid']) == 0 || (count($options['bannerid']) == 1 && $options['bannerid'][0] == ""))
            )) {

            throw new \Exception('Error: At least one pidm, one uniqueid or one bannerid must be specified.');
        }

        //keep track of how many person paramters were sent
        $numberOfPersonParams = 0;

        //check for and get the pidm option
        if ($options && isset($options['pidm']) && count($options['pidm']) > 0) {
            $pidmParam = $options['pidm'];
            $numberOfPersonParams++;
        }

        //check for and get the uniqueid option
        if ($options && isset($options['uniqueid']) && count($options['uniqueid']) > 0) {
            $uniqueidParam = $this->upperCaseUniqueids($options['uniqueid']);
            $numberOfPersonParams++;
        }

        //check for and get the bannerid option
        if ($options && isset($options['bannerid']) && count($options['bannerid']) > 0) {
            $banneridParam = $options['bannerid'];
            $numberOfPersonParams++;
        }

        //check for and get the fundTypeCode option
        if (array_key_exists('fundTypeCode', $options)) {
            $fundTypeCode = $options['fundTypeCode'];
        }

        //only allow one filter for person: uniqueid, bannerid or pidm
        if ($numberOfPersonParams != 1) {
            throw new \Exception("Error: Only one of the follow parameters may be submitted at a time - uniqueid, bannerid and pidm.");
        }

        //Get Awards for any given pidms and add to the payload
        if (isset($pidmParam)) {
            if (count($options['pidm']) > 0 && preg_match('/^\\d{3,}( \\d{3,})*$/', implode(' ', $options['pidm'])) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Pidms must be numeric.');
            }
            $pidmRecords = $this->buildGetPayloadForPidms($this->getAwardDataWithPidms($pidmParam, $fundTypeCode), $pidmParam);
            foreach ($pidmRecords as $p => $v) { //add each array to the payload
                $payload[$p] = $v;
            }
        }

        //Get Awards for any given uniqueids and add to the payload
        if (isset($uniqueidParam) && count($uniqueidParam) > 0) {
            if (count($options['uniqueid']) > 0 && preg_match('/^[[:alnum:]]{3,}$/', implode('', $options['uniqueid'])) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Unique IDs must only contain Numbers and Letters.');
            }
            $uniqueidRecords = $this->buildGetPayloadForUniqueids($this->getAwardDataWithUniqueids($uniqueidParam, $fundTypeCode), $uniqueidParam);
            foreach ($uniqueidRecords as $u => $v) { //add each array to the payload
                $payload[$u] = $v;
            }
        }

        //Get Awards for any given bannerids and add to the payload
        if (isset($banneridParam) && count($banneridParam) > 0) {
            if (count($options['bannerid']) > 0 && preg_match('/^\\d{8}( \\d{8})*$/', implode(' ', $options['bannerid'])) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Banner ID must be 8 digits.');
            }
            $banneridRecords = $this->buildGetPayloadForBannerids($this->getAwardDataWithBannerids($banneridParam, $fundTypeCode), $banneridParam);
            foreach ($banneridRecords as $u => $v) { //add each array to the payload
                $payload[$u] = $v;
            }
        }

        //Response was successful and Return information
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;

    }

    /*
     * Helper Method to Get Award Information for PIDMs
     *
     * Parameters:
     * pidms: PIDMs to be Retrived
     *
     * Return: Returns an array of all awards of the PIDMs requested.
     */
    private function getAwardDataWithPidms($pidms, $fundTypeCode)
    {
        try {
            //proceed if we were given pidms to work with
            if ($pidms) {
                $dbh = $this->database->getHandle($this->datasource_name);
                $queryString = "
					select
						szbuniq_unique_id,				--uniqueid
						szbuniq_banner_id,				--bannerid
						awrd.rprawrd_pidm,              --pidm
						awrd.rprawrd_aidy_code,         --aidYearCode
						awrd.rprawrd_fund_code,         --fundCode
						base.rfrbase_fund_title,        --fundTitle
						base.rfrbase_fsrc_code,         --fsrcCode
						fsrc.rtvfsrc_desc,              --fsrcDescription
						fsrc.rtvfsrc_ind,               --fsrcIndicator
						base.rfrbase_ftyp_code,         --fundTypeCode
						ftyp.rtvftyp_desc,              --fundTypeDescription
						awrd.rprawrd_awst_code,         --awstCode
						awst.rtvawst_desc,              --awstDescription
						awrd.rprawrd_awst_date,         --awstDate
						awrd.rprawrd_sys_ind,           --systemIndicator
						awrd.rprawrd_activity_date,     --activityDate
						awrd.rprawrd_lock_ind,          --lockIndicator
						awrd.rprawrd_offer_exp_date,    --offerExpirationDate
						awrd.rprawrd_accept_amt,        --acceptAmount
						awrd.rprawrd_accept_date,       --acceptDate
						awrd.rprawrd_authorize_amt,     --authorizeAmount
						awrd.rprawrd_authorize_date,    --authorizeDate
						awrd.rprawrd_memo_amt,          --memoAmount
						awrd.rprawrd_memo_date,         --memoDate
						awrd.rprawrd_paid_amt,          --paidAmount
						awrd.rprawrd_paid_date,         --paidDate
						awrd.rprawrd_orig_offer_amt,    --originalOfferAmount
						awrd.rprawrd_orig_offer_date,   --originalOfferDate
						awrd.rprawrd_offer_amt,         --offerAmount
						awrd.rprawrd_offer_date,        --offerDate
						awrd.rprawrd_decline_amt,       --declineAmount
						awrd.rprawrd_decline_date,      --declineDate
						awrd.rprawrd_cancel_amt,        --cancelAmount
						awrd.rprawrd_cancel_date        --cancelDate
					from 
						rprawrd awrd
						inner join rfrbase base
							on awrd.rprawrd_fund_code = base.rfrbase_fund_code
						inner join rtvftyp ftyp
							on base.rfrbase_ftyp_code = ftyp.rtvftyp_code
						inner join rtvfsrc fsrc 
							on base.rfrbase_fsrc_code = fsrc.rtvfsrc_code
						inner join rtvawst awst 
							on awrd.rprawrd_awst_code = awst.rtvawst_code
						inner join szbuniq
							on szbuniq_pidm = awrd.rprawrd_pidm
					where awrd.rprawrd_pidm in (
				";

                //loop through and add each pidm to the in statement if they are numeric
                foreach ($pidms as $pidm) {
                    if (is_numeric($pidm)) {
                        $queryString .= $pidm . ",";
                    } else {
                        throw new \Exception('Pidms must be numeric: ' . $pidm);
                    }
                }

                //remove the extra comma at the end of the pidms
                $queryString = substr($queryString, 0, -1);
                //add the ending paren
                $queryString .= ')';


                //execute the query
                if ($fundTypeCode) { //if we got a fundTypeCode filter
                    $queryString .= " and base.rfrbase_ftyp_code = ?";
                    $results = $dbh->queryall_array($queryString, $fundTypeCode);
                } else { //else execute without the filter
                    $results = $dbh->queryall_array($queryString);
                }


            } else {
                throw new \Exception('Pidm(s) are required to get refunds. ');
            }

        } catch (\Exception $e) {
            throw new \Exception('Error getting data: ' . $e);
        }

        return $results;
    }

    /*
    * Helper Method to Get Award Information for Uniqueids
    *
    * Parameters:
    * pidms: Uniqueids to be Retrived
    *
    * Return: Returns an array of all awards of the Uniqueids requested.
    */
    private function getAwardDataWithUniqueIds($uniqueids, $fundTypeCode)
    {
        try {
            //proceed if we were given uniqueids to work with
            if ($uniqueids) {
                $dbh = $this->database->getHandle($this->datasource_name);
                $queryString = "
					select
						szbuniq_unique_id,				--uniqueid
						szbuniq_banner_id,				--bannerid
						awrd.rprawrd_pidm,              --pidm
						awrd.rprawrd_aidy_code,         --aidYearCode
						awrd.rprawrd_fund_code,         --fundCode
						base.rfrbase_fund_title,        --fundTitle
						base.rfrbase_fsrc_code,         --fsrcCode
						fsrc.rtvfsrc_desc,              --fsrcDescription
						fsrc.rtvfsrc_ind,               --fsrcIndicator
						base.rfrbase_ftyp_code,         --fundTypeCode
						ftyp.rtvftyp_desc,              --fundTypeDescription
						awrd.rprawrd_awst_code,         --awstCode
						awst.rtvawst_desc,              --awstDescription
						awrd.rprawrd_awst_date,         --awstDate
						awrd.rprawrd_sys_ind,           --systemIndicator
						awrd.rprawrd_activity_date,     --activityDate
						awrd.rprawrd_lock_ind,          --lockIndicator
						awrd.rprawrd_offer_exp_date,    --offerExpirationDate
						awrd.rprawrd_accept_amt,        --acceptAmount
						awrd.rprawrd_accept_date,       --acceptDate
						awrd.rprawrd_authorize_amt,     --authorizeAmount
						awrd.rprawrd_authorize_date,    --authorizeDate
						awrd.rprawrd_memo_amt,          --memoAmount
						awrd.rprawrd_memo_date,         --memoDate
						awrd.rprawrd_paid_amt,          --paidAmount
						awrd.rprawrd_paid_date,         --paidDate
						awrd.rprawrd_orig_offer_amt,    --originalOfferAmount
						awrd.rprawrd_orig_offer_date,   --originalOfferDate
						awrd.rprawrd_offer_amt,         --offerAmount
						awrd.rprawrd_offer_date,        --offerDate
						awrd.rprawrd_decline_amt,       --declineAmount
						awrd.rprawrd_decline_date,      --declineDate
						awrd.rprawrd_cancel_amt,        --cancelAmount
						awrd.rprawrd_cancel_date        --cancelDate
					from 
						rprawrd awrd
						inner join rfrbase base
							on awrd.rprawrd_fund_code = base.rfrbase_fund_code
						inner join rtvftyp ftyp
							on base.rfrbase_ftyp_code = ftyp.rtvftyp_code
						inner join rtvfsrc fsrc 
							on base.rfrbase_fsrc_code = fsrc.rtvfsrc_code
						inner join rtvawst awst 
							on awrd.rprawrd_awst_code = awst.rtvawst_code
						inner join szbuniq
							on szbuniq_pidm = awrd.rprawrd_pidm
					where szbuniq_unique_id in (
				";

                //loop through and add each uniqueid to the in statement
                foreach ($uniqueids as $u) {
                    $queryString .= "'" . $u . "',";
                }

                //remove the extra comma at the end of the pidms
                $queryString = substr($queryString, 0, -1);
                //add the ending paren
                $queryString .= ')';


                //execute the query
                if ($fundTypeCode) { //if we got a fundTypeCode filter
                    $queryString .= " and base.rfrbase_ftyp_code = ?";
                    $results = $dbh->queryall_array($queryString, $fundTypeCode);
                } else { //else execute without the filter
                    $results = $dbh->queryall_array($queryString);
                }


            } else {
                throw new \Exception('Uniqueids(s) are required to get awards.');
            }

        } catch (\Exception $e) {
            throw new \Exception('Error getting data: ' . $e);
        }

        return $results;
    }

    /*
    * Helper Method to Get Award Information for Bannerids
    *
    * Parameters:
    * pidms: Bannerids to be Retrived
    *
    * Return: Returns an array of all awards of the Bannerids requested.
    */
    private function getAwardDataWithBannerids($bannerids, $fundTypeCode)
    {
        try {
            //proceed if we were given uniqueids to work with
            if ($bannerids) {
                $dbh = $this->database->getHandle($this->datasource_name);
                $queryString = "
					select
						szbuniq_unique_id,				--uniqueid
						szbuniq_banner_id,				--bannerid
						awrd.rprawrd_pidm,              --pidm
						awrd.rprawrd_aidy_code,         --aidYearCode
						awrd.rprawrd_fund_code,         --fundCode
						base.rfrbase_fund_title,        --fundTitle
						base.rfrbase_fsrc_code,         --fsrcCode
						fsrc.rtvfsrc_desc,              --fsrcDescription
						fsrc.rtvfsrc_ind,               --fsrcIndicator
						base.rfrbase_ftyp_code,         --fundTypeCode
						ftyp.rtvftyp_desc,              --fundTypeDescription
						awrd.rprawrd_awst_code,         --awstCode
						awst.rtvawst_desc,              --awstDescription
						awrd.rprawrd_awst_date,         --awstDate
						awrd.rprawrd_sys_ind,           --systemIndicator
						awrd.rprawrd_activity_date,     --activityDate
						awrd.rprawrd_lock_ind,          --lockIndicator
						awrd.rprawrd_offer_exp_date,    --offerExpirationDate
						awrd.rprawrd_accept_amt,        --acceptAmount
						awrd.rprawrd_accept_date,       --acceptDate
						awrd.rprawrd_authorize_amt,     --authorizeAmount
						awrd.rprawrd_authorize_date,    --authorizeDate
						awrd.rprawrd_memo_amt,          --memoAmount
						awrd.rprawrd_memo_date,         --memoDate
						awrd.rprawrd_paid_amt,          --paidAmount
						awrd.rprawrd_paid_date,         --paidDate
						awrd.rprawrd_orig_offer_amt,    --originalOfferAmount
						awrd.rprawrd_orig_offer_date,   --originalOfferDate
						awrd.rprawrd_offer_amt,         --offerAmount
						awrd.rprawrd_offer_date,        --offerDate
						awrd.rprawrd_decline_amt,       --declineAmount
						awrd.rprawrd_decline_date,      --declineDate
						awrd.rprawrd_cancel_amt,        --cancelAmount
						awrd.rprawrd_cancel_date        --cancelDate
					from 
						rprawrd awrd
						inner join rfrbase base
							on awrd.rprawrd_fund_code = base.rfrbase_fund_code
						inner join rtvftyp ftyp
							on base.rfrbase_ftyp_code = ftyp.rtvftyp_code
						inner join rtvfsrc fsrc 
							on base.rfrbase_fsrc_code = fsrc.rtvfsrc_code
						inner join rtvawst awst 
							on awrd.rprawrd_awst_code = awst.rtvawst_code
						inner join szbuniq
							on szbuniq_pidm = awrd.rprawrd_pidm
					where szbuniq_banner_id in (
				";

                //loop through and add each bannerid to the in statement
                foreach ($bannerids as $b) {
                    $queryString .= "'+" . $b . "',";
                }


                //remove the extra comma at the end of the pidms
                $queryString = substr($queryString, 0, -1);
                //add the ending paren
                $queryString .= ')';


                //execute the query
                if ($fundTypeCode) { //if we got a fundTypeCode filter
                    $queryString .= " and base.rfrbase_ftyp_code = ?";
                    $results = $dbh->queryall_array($queryString, $fundTypeCode);
                } else { //else execute without the filter
                    $results = $dbh->queryall_array($queryString);
                }


            } else {
                throw new \Exception('Bannerid(s) are required to get awards.');
            }

        } catch (\Exception $e) {
            throw new \Exception('Error getting data: ' . $e);
        }

        return $results;
    }

    /*
     * Helper to build return payload for pidms.
     *
     * Parameters:
     * awardData: Associtive array of address data to process.
     * pidms: array of pidms
     *
     * Return: Associtive Array of data that should be returned.
     */
    private function buildGetPayloadForPidms($awardData, $pidms)
    {
        $records = array();

        //initialize a sub-array for each pidm given so that if they had no awards,
        //we still get a node for that pidm anyway
        foreach ($pidms as $pidm) {
            $records[$pidm] = array();
        }

        $prettyAwardData = $this->awardMakeover($awardData);
        foreach ($prettyAwardData as $award) {
            $records[$award['pidm']][$award['aidYearCode']][$award['fundCode']] = $award;
        }
        return $records;
    }

    /*
    * Helper to build return payload for uniqueids.
    *
    * Parameters:
    * awardData: Associtive array of award data to process.
    * uniqueids: array of uniqueids
    *
    * Return: Associtive Array of data that should be returned.
    */
    private function buildGetPayloadForUniqueids($awardData, $uniqueids)
    {
        $records = array();

        //initialize a sub-array for each uniqueid given so that if they had no awards,
        //we still get a node for that pidm anyway
        foreach ($uniqueids as $u) {
            $records[$u] = array();
        }

        $prettyAwardData = $this->awardMakeover($awardData);
        foreach ($prettyAwardData as $award) {
            $records[$award['uniqueid']][$award['aidYearCode']][$award['fundCode']] = $award;
        }
        return $records;
    }

    /*
    * Helper to build return payload for bannerids.
    *
    * Parameters:
    * awardData: Associtive array of address data to process.
    * pidms: array of bannerids
    *
    * Return: Associtive Array of data that should be returned.
    */
    private function buildGetPayloadForBannerids($awardData, $bannerids)
    {
        $records = array();

        //initialize a sub-array for each bannerid given so that if they had no awards,
        //we still get a node for that bannerid anyway
        foreach ($bannerids as $b) {
            $records[$b] = array();
        }

        $prettyAwardData = $this->awardMakeover($awardData);
        foreach ($prettyAwardData as $award) {
            $records[$award['bannerid']][$award['aidYearCode']][$award['fundCode']] = $award;
        }
        return $records;
    }

    /*
    * Helper method to turn ugly awardData into pretty awardData.
    * So beautiful.  The most prettiest data ever.
    */
    private function awardMakeover($awardData)
    {
        $prettyArray = array();
        foreach ($awardData as $award) {
            $prettyAward = array();
            $prettyAward['uniqueid'] = (isset($award['szbuniq_unique_id']) ? $award['szbuniq_unique_id'] : NULL);
            $prettyAward['bannerid'] = (substr($award['szbuniq_banner_id'], 1) !== NULL ? substr($award['szbuniq_banner_id'], 1) : NULL);
            $prettyAward['pidm'] = (isset($award['rprawrd_pidm']) ? $award['rprawrd_pidm'] : NULL);
            $prettyAward['aidYearCode'] = (isset($award['rprawrd_aidy_code']) ? $award['rprawrd_aidy_code'] : NULL);
            $prettyAward['fundCode'] = (isset($award['rprawrd_fund_code']) ? $award['rprawrd_fund_code'] : NULL);
            $prettyAward['fundTitle'] = (isset($award['rfrbase_fund_title']) ? $award['rfrbase_fund_title'] : NULL);
            $prettyAward['fsrcCode'] = (isset($award['rfrbase_fsrc_code']) ? $award['rfrbase_fsrc_code'] : NULL);
            $prettyAward['fsrcDescription'] = (isset($award['rtvfsrc_desc']) ? $award['rtvfsrc_desc'] : NULL);
            $prettyAward['fsrcIndicator'] = (isset($award['rtvfsrc_ind']) ? $award['rtvfsrc_ind'] : NULL);
            $prettyAward['fundTypeCode'] = (isset($award['rfrbase_ftyp_code']) ? $award['rfrbase_ftyp_code'] : NULL);
            $prettyAward['fundTypeDescription'] = (isset($award['rtvftyp_desc']) ? $award['rtvftyp_desc'] : NULL);
            $prettyAward['awstCode'] = (isset($award['rprawrd_awst_code']) ? $award['rprawrd_awst_code'] : NULL);
            $prettyAward['awstDescription'] = (isset($award['rtvawst_desc']) ? $award['rtvawst_desc'] : NULL);
            $prettyAward['awstDate'] = (isset($award['rprawrd_awst_date']) ? $award['rprawrd_awst_date'] : NULL);
            $prettyAward['systemIndicator'] = (isset($award['rprawrd_sys_ind']) ? $award['rprawrd_sys_ind'] : NULL);
            $prettyAward['activityDate'] = (isset($award['rprawrd_activity_date']) ? $award['rprawrd_activity_date'] : NULL);
            $prettyAward['lockIndicator'] = (isset($award['rprawrd_lock_ind']) ? $award['rprawrd_lock_ind'] : NULL);
            $prettyAward['offerExpirationDate'] = (isset($award['rprawrd_offer_exp_date']) ? $award['rprawrd_offer_exp_date'] : NULL);
            $prettyAward['acceptAmount'] = (isset($award['rprawrd_accept_amt']) ? $award['rprawrd_accept_amt'] : NULL);
            $prettyAward['acceptDate'] = (isset($award['rprawrd_accept_date']) ? $award['rprawrd_accept_date'] : NULL);
            $prettyAward['authorizeAmount'] = (isset($award['rprawrd_authorize_amt']) ? $award['rprawrd_authorize_amt'] : NULL);
            $prettyAward['authorizeDate'] = (isset($award['rprawrd_authorize_date']) ? $award['rprawrd_authorize_date'] : NULL);
            $prettyAward['memoAmount'] = (isset($award['rprawrd_memo_amt']) ? $award['rprawrd_memo_amt'] : NULL);
            $prettyAward['memoDate'] = (isset($award['rprawrd_memo_date']) ? $award['rprawrd_memo_date'] : NULL);
            $prettyAward['paidAmount'] = (isset($award['rprawrd_paid_amt']) ? $award['rprawrd_paid_amt'] : NULL);
            $prettyAward['paidDate'] = (isset($award['rprawrd_paid_date']) ? $award['rprawrd_paid_date'] : NULL);
            $prettyAward['originalOfferAmount'] = (isset($award['rprawrd_orig_offer_amt']) ? $award['rprawrd_orig_offer_amt'] : NULL);
            $prettyAward['originalOfferDate'] = (isset($award['rprawrd_orig_offer_date']) ? $award['rprawrd_orig_offer_date'] : NULL);
            $prettyAward['offerAmount'] = (isset($award['rprawrd_offer_amt']) ? $award['rprawrd_offer_amt'] : NULL);
            $prettyAward['offerDate'] = (isset($award['rprawrd_offer_date']) ? $award['rprawrd_offer_date'] : NULL);
            $prettyAward['declineAmount'] = (isset($award['rprawrd_decline_amt']) ? $award['rprawrd_decline_amt'] : NULL);
            $prettyAward['declineDate'] = (isset($award['rprawrd_decline_date']) ? $award['rprawrd_decline_date'] : NULL);
            $prettyAward['cancelAmount'] = (isset($award['rprawrd_cancel_amt']) ? $award['rprawrd_cancel_amt'] : NULL);
            $prettyAward['cancelDate'] = (isset($award['rprawrd_cancel_date']) ? $award['rprawrd_cancel_date'] : NULL);
            $prettyArray[] = $prettyAward;
        }
        return $prettyArray;
    }

    /*
    * Helper method to convert uniqueids to upper for safety.
    */
    private function upperCaseUniqueids($uniqueids)
    {
        $upperUniqueids = array();
        foreach ($uniqueids as $u) {
            $upperUniqueids[] = strtoupper($u);
        }
        return $upperUniqueids;
    }
}