<?php

namespace MiamiOH\FinancialAidRest\Award\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class AwardResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Award.Inner',
            'type' => 'object',
            'properties' => array(
                'uniqueid' => array(
                    'type' => 'string',
                ),
                'bannerid' => array(
                    'type' => 'number',
                ),
                'pidm' => array(
                    'type' => 'number',
                ),
                'aidYearCode' => array(
                    'type' => 'number',
                ),
                'fundCode' => array(
                    'type' => 'string',
                ),
                'fundTitle' => array(
                    'type' => 'string',
                ),
                'fsrcCode' => array(
                    'type' => 'string',
                ),
                'fsrcDescription' => array(
                    'type' => 'string',
                ),
                'fsrcIndicator' => array(
                    'type' => 'string',
                ),
                'fundTypeCode' => array(
                    'type' => 'string',
                ),
                'fundTypeDescription' => array(
                    'type' => 'string',
                ),
                'awstCode' => array(
                    'type' => 'string',
                ),
                'awstDescription' => array(
                    'type' => 'string',
                ),
                'awstDate' => array(
                    'type' => 'string',
                ),
                'systemIndicator' => array(
                    'type' => 'string',
                ),
                'activityDate' => array(
                    'type' => 'string',
                ),
                'lockIndicator' => array(
                    'type' => 'string',
                ),
                'offerExpirationDate' => array(
                    'type' => 'string',
                ),
                'acceptAmout' => array(
                    'type' => 'number',
                ),
                'acceptDate' => array(
                    'type' => 'string',
                ),
                'memoAmount' => array(
                    'type' => 'number',
                ),
                'memoDate' => array(
                    'type' => 'string',
                ),
                'paidAmount' => array(
                    'type' => 'number',
                ),
                'paidDate' => array(
                    'type' => 'string',
                ),
                'originalOfferAmount' => array(
                    'type' => 'number',
                ),
                'originalOfferDate' => array(
                    'type' => 'string',
                ),
                'offerAmount' => array(
                    'type' => 'number',
                ),
                'offerDate' => array(
                    'type' => 'string',
                ),
                'declineAmount' => array(
                    'type' => 'number',
                ),
                'declineDate' => array(
                    'type' => 'string',
                ),
                'cancelAmount' => array(
                    'type' => 'number',
                ),
                'cancelDate' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Award.Inner.Collection',
            'type' => 'array',
            '$ref' => '#/definitions/Student.FinancialAid.Award.Inner',
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Award.FundCodes',
            'type' => 'object',
            'properties' => array(
                'fundCodeKey' => array(
                    'type' => 'array',
                    'items' => array(
                        '$ref' => '#/definitions/Student.FinancialAid.Award.Inner.Collection'
                    ),
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Award.FundCodes.Collection',
            'type' => 'array',
            '$ref' => '#/definitions/Student.FinancialAid.Award.FundCodes',
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Award.AidYears',
            'type' => 'object',
            'properties' => array(
                'aidYearKey' => array(
                    'type' => 'array',
                    'items' => array(
                        '$ref' => '#/definitions/Student.FinancialAid.Award.FundCodes.Collection'
                    ),
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Award.AidYears.Collection',
            'type' => 'array',
            '$ref' => '#/definitions/Student.FinancialAid.Award.AidYears',
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Award.SearchKey',
            'type' => 'object',
            'properties' => array(
                'searchKey' => array(
                    'type' => 'array',
                    'items' => array(
                        '$ref' => '#/definitions/Student.FinancialAid.Award.AidYears.Collection',
                    ),
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Award.SearchKey.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Student.FinancialAid.Award.SearchKey',
            ),
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Award',
            'class' => 'MiamiOH\FinancialAidRest\Award\Services\Award',
            'description' => 'Information about awards',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'dataSource' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(
            array(
                'action' => 'read',
                'name' => 'student.financialAid.award.get',
                'description' => "Read a student's awards",
                'pattern' => '/student/financialAid/award/v1',
                'service' => 'Award',
                'method' => 'getAward',
                'isPageable' => false,
                'tags' => array('Student'),
                'returnType' => 'collection',
                'options' => array(
                    'pidm' => array('type' => 'list', 'description' => 'pidm(s) to get award records'),
                    'uniqueid' => array('type' => 'list', 'description' => 'uniqueid(s) to get award records'),
                    'bannerid' => array('type' => 'list', 'description' => 'bannerid(s) to get award records.  They should include 8 digits and no + character.'),
                    'fundTypeCode' => array('type' => 'single', 'required' => false, 'description' => 'Fund type code of the award. Ex: LOAN, GRNT, SCHM, etc.'),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array('type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'StudentFinancialAid',
                            'key' => array('view')
                        ),
                    ),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'A collection of award records',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/Student.FinancialAid.Award.SearchKey.Collection',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}