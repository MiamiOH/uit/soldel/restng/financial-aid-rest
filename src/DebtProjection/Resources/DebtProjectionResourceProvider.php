<?php

namespace MiamiOH\FinancialAidRest\DebtProjection\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class DebtProjectionResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'FinancialAid',
            'description' => 'Resources for Financial Aid Information about Students'
        ));


        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.DebtProjection.TotalFinancialAidByAidYear',
            'type' => 'object',
            'properties' => array(
                'aidYear' => array(
                    'type' => 'number',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.DebtProjection.TotalFinancialAidByAidYear.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Student.FinancialAid.DebtProjection.TotalFinancialAidByAidYear'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.DebtProjection',
            'type' => 'object',
            'properties' => array(
                'universityLoanSubTotal' => array(
                    'type' => 'number',
                ),
                'federalLoanSubTotal' => array(
                    'type' => 'number',
                ),
                'privateLoanSubTotal' => array(
                    'type' => 'number',
                ),
                'perkinsLoanSubTotal' => array(
                    'type' => 'number',
                ),
                'monthlyPayment' => array(
                    'type' => 'number',
                ),
                'cumulativeTotal' => array(
                    'type' => 'number',
                ),
                'totalInterest' => array(
                    'type' => 'number',
                ),
                'totalFinancialAidByAidYear' => array(
                    'type' => 'array',
                    '$ref' => '#/definitions' . 'Student.FinancialAid.DebtProjection.TotalFinancialAidByAidYear.Collection',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.DebtProjection.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Student.FinancialAid.DebtProjection'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(

            'name' => 'DebtProjection',
            'class' => 'MiamiOH\FinancialAidRest\DebtProjection\Services\DebtProjection',
            'description' => 'Information about a persons projected debt',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'dataSource' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(

                'action' => 'read',
                'name' => 'student.financialAid.debtProjection.get',
                'description' => 'Read a student projected debt.',
                'pattern' => '/student/financialAid/debtProjection/v1',
                'service' => 'DebtProjection',
                'method' => 'getDebtProjection',
                'isPageable' => false,
                'tags' => array('Student'),
                'returnType' => 'collection',
                'options' => array(
                    'pidm' => array('type' => 'list', 'required' => false, 'description' => 'PIDM(s) to get debt projection records. Cannot be used in combination with uniqueid or bannerid parameters.'),
                    'uniqueid' => array('type' => 'list', 'required' => false, 'description' => 'Unique ID(s) to get debt projection records. Cannot be used in combination with pidm or bannerid parameters.'),
                    'bannerid' => array('type' => 'list', 'required' => false, 'description' => 'Banner ID(s) to get debt projection records. Cannot be used in combination with pidm or uniqueid parameters.'),
                    'interestRate' => array('type' => 'single', 'required' => false, 'description' => 'Interest Rate for Monthly Payment Calculation as a percentage. Requires numberOfYears Parameter to be set.'),
                    'numberOfYears' => array('type' => 'single', 'required' => false, 'description' => 'Number of years for Monthly Payment Calculation. Requires interestRate Parameter to be set.'),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array('type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'StudentFinancialAid',
                            'key' => array('view')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'A collection of projected debt',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/Student.FinancialAid.DebtProjection.Collection',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}