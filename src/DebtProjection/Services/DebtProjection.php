<?php
/*
-----------------------------------------------------------
FILE NAME: DebtProjection.class.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Erin Mills

DESCRIPTION:  The email service is designed to get, update and insert phone records to banner goremal table
When doing get, pidm or uniqueId is required
When doing post, pidm and sequence number is needed

De-dup: The service will first check if there are duplicate records before doing an insert, in the event that
it finds duplicate records, an error message will be returned to consumer app

Authorization: authentication and authorization are handled by authorization token. The Service will treat
phone data as public but when people insert new records, the consumer app would need update right of person
project in Authman

Post without sequence number: if the record being posted does not have duplicate in database, and the consumer
app did not provide seqence number, the service will treat this a new valiad phone number and will add up one
to the current sequence number and perform insert

Protected group: SPBPERS_CONFID_IND = 'Y'. We will not update/get/insert any data for this people. Error message will
not be sent out but the consumer app should know the feature of this service.

INPUT:
PARAMETERS: pidm, sequence number and uniqueId

ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

TABLE USAGE:

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

02/XX/2016              SCHMIDEE
Description:  Initial Program

 */

namespace MiamiOH\FinancialAidRest\DebtProjection\Services;

class DebtProjection extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD';

    //	Helper functions that were called by the frame work and create internal datasource and configuration objects
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /*
     * Get email records for a pidm
     */
    public function getDebtProjection()
    {

        //log
        $this->log->debug('  DebtProjection:getDebtProjection() was called.');

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();
        $additionalwhere = null;
        $pidms = array();

        //Confirm User Can actually Access this Information
        /*$user = $this->getApiUser();
        $authorized = $user->isAuthorized('WebServices', 'Person', 'All');
        if(!$authorized){
            $authorized = $user->isAuthorized('WebServices', 'Person', 'view');
        }

        if (!$authorized) {
          $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
          return $response;
        }*/

        //get parameters from user input (url)

        if ((isset($options['pidm']) && is_array($options['pidm']) && count($options['pidm']) > 0)
            || (isset($options['uniqueid']) && is_array($options['uniqueid']) && count($options['uniqueid']) > 0)
            || (isset($options['bannerid']) && is_array($options['bannerid']) && count($options['bannerid']) > 0)
        ) { //if pidms or uniqueids are given
            $awardGetArray = array();
            $notSetCount = 0;
            if (!isset($options['uniqueid'])) {
                $options['uniqueid'] = array();
                $notSetCount++;
            }

            if (!isset($options['pidm'])) {
                $options['pidm'] = array();
                $notSetCount++;
            }

            if (!isset($options['bannerid'])) {
                $options['bannerid'] = array();
                $notSetCount++;
            }
            if ($notSetCount < 2) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Please only search by PIDM, UniqueID or BannerID, not multiples.');
            }

            $options['uniqueid'] = array_filter($options['uniqueid']);
            $options['pidm'] = array_filter($options['pidm']);
            $options['bannerid'] = array_filter($options['bannerid']);

            if (count($options['pidm']) > 0 && preg_match('/^\\d{3,}( \\d{3,})*$/', implode(' ', $options['pidm'])) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Pidms must be numeric.');
            } else {
                if (isset($options['pidm']) && count($options['pidm']) > 0) {
                    $awardGetArray['pidm'] = implode(',', $options['pidm']);
                }
            }
            if (count($options['bannerid']) > 0 && preg_match('/^\\d{8}( \\d{8})*$/', implode(' ', $options['bannerid'])) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Banner ID must be 8 digits.');
            } else {
                if (isset($options['bannerid']) && count($options['bannerid']) > 0) {
                    $awardGetArray['bannerid'] = implode(',', $options['bannerid']);
                }
            }

            if (count($options['uniqueid']) > 0 && preg_match('/^[[:alnum:]]{3,}$/', implode('', $options['uniqueid'])) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Unique IDs must only contain Numbers and Letters.');
            } else {
                if (isset($options['uniqueid']) && count($options['uniqueid']) > 0) {
                    $awardGetArray['uniqueid'] = implode(',', $options['uniqueid']);
                }
            }

            if (isset($options['interestRate']) && preg_match('/^\-?\d+(\.\d+)?$/', $options['interestRate']) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Interest Rate must be numeric.');
            }

            if (isset($options['numberOfYears']) && preg_match('/^\-?\d+$/', $options['numberOfYears']) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Number of Years must be numeric.');
            }

            $awardGetArray['fundTypeCode'] = 'LOAN';

            //Call FinacialAid/Award Service for all requrested PIDMS
            $responses = $this->callResource('student.financialAid.award.get', array('options' => $awardGetArray));

            //Process the returned Elements
            $returnArray = $this->processAwardElements($responses->getPayload(), (isset($options['interestRate']) ? $options['interestRate'] : NULL), (isset($options['numberOfYears']) ? $options['numberOfYears'] : NULL));

            //Set the formatted response as payload
            $payload = $returnArray;

        } else {
            throw new \Exception('Error: At least one pidm, one uniqueid, or one bannerid must be specified.');
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;

    }

    private function processAwardElements($payload, $interestRate, $numberOfYears)
    {
        $returnArray = array();
        //print "----------------\n";
        foreach ($payload as $student_key => $student_array) {
            //Create Default Element
            $returnArray[$student_key] = array(
                'perkinsLoanSubTotal' => 0,
                'universityLoanSubTotal' => 0,
                'federalLoanSubTotal' => 0,
                'privateLoanSubTotal' => 0,
                'loanTotal' => 0,
                'totalFinancialAidByAidYear' => array(),
            );
            foreach ($student_array as $year_key => $year_array) {
                $perkinsLoanSubTotal = 0;
                $universityLoanSubTotal = 0;
                $federalLoanSubTotal = 0;
                $privateLoanSubTotal = 0;
                foreach ($year_array as $fund_array) {
                    if ($fund_array['paidAmount'] > 0) {
                        //Valid Paid Amount
                        switch (strtoupper($fund_array['fundCode'])) {
                            //Check for Fund Code Specific Loans
                            case 'PERK':
                                //Perkin's Loan Found
                                $perkinsLoanSubTotal += $fund_array['acceptAmount'];
                                break;
                            case 'PLUS':
                                //PLUS Loan Found (Ignore so it doesn't get counted in Federal Loan Count)
                                break;
                            default:
                                //Check for fsrcIndicator Specifc Loans
                                switch (strtoupper($fund_array['fsrcIndicator'])) {
                                    case 'I':
                                        //University Loan Found
                                        $universityLoanSubTotal += $fund_array['acceptAmount'];
                                        break;
                                    case 'F':
                                        //Federal Loan Found
                                        $federalLoanSubTotal += $fund_array['acceptAmount'];
                                        break;
                                    case 'O':
                                        //Private Loan Found
                                        $privateLoanSubTotal += $fund_array['offerAmount'];
                                        break;
                                }
                                break;
                        }
                    } else {
                        /*
                         * No Paid Amount. It turns out private loans do not ever get 
                         * listed as paid. 
                         */
                        switch (strtoupper($fund_array['fsrcIndicator'])) {
                            case 'O':
                                //Private Loan Found
                                $privateLoanSubTotal += $fund_array['offerAmount'];
                                break;
                        }
                    }
                }
                /*print "Aid Year: ".$year_key."\n";
                print "Perkins: ".$perkinsLoanSubTotal."\n";
                print "University: ".$universityLoanSubTotal."\n";
                print "Federal: ".$federalLoanSubTotal."\n";
                print "Private: ".$privateLoanSubTotal."\n\n";*/
                $returnArray[$student_key]['perkinsLoanSubTotal'] += $perkinsLoanSubTotal;
                $returnArray[$student_key]['universityLoanSubTotal'] += $universityLoanSubTotal;
                $returnArray[$student_key]['federalLoanSubTotal'] += $federalLoanSubTotal;
                $returnArray[$student_key]['privateLoanSubTotal'] += $privateLoanSubTotal;
                if ($perkinsLoanSubTotal + $universityLoanSubTotal + $federalLoanSubTotal + $privateLoanSubTotal > 0) {
                    $returnArray[$student_key]['totalFinancialAidByAidYear'][$year_key] = $perkinsLoanSubTotal +
                        $universityLoanSubTotal +
                        $federalLoanSubTotal +
                        $privateLoanSubTotal;
                    //print "Total: ".$returnArray[$student_key]['totalFinancialAidByAidYear'][$year_key]."\n\n";

                }

            }

            //Create Total for all Loans
            $returnArray[$student_key]['loanTotal'] += $returnArray[$student_key]['universityLoanSubTotal'] +
                $returnArray[$student_key]['federalLoanSubTotal'] +
                $returnArray[$student_key]['privateLoanSubTotal'] +
                $returnArray[$student_key]['perkinsLoanSubTotal'];

            if ($interestRate != NULL && $numberOfYears != NULL) {
                $numberOfPayments = intval($numberOfYears) * 12.00;
                $i = $interestRate / 1200.0;
                $t = $i * -$returnArray[$student_key]['loanTotal'] * pow((1 + $i), $numberOfPayments) / (1 - pow((1 + $i), $numberOfPayments));

                $returnArray[$student_key]['monthlyPayment'] = number_format($t, 2, '.', '');
                $returnArray[$student_key]['cumulativeTotal'] = number_format($returnArray[$student_key]['monthlyPayment'] * $numberOfPayments, 2, '.', '');
                $returnArray[$student_key]['totalInterest'] = number_format($returnArray[$student_key]['cumulativeTotal'] - $returnArray[$student_key]['loanTotal'], 2, '.', '');
            }
        }

        //Return Result
        return $returnArray;
    }


    private function buildReturnElements($resultsArray)
    {
        $returnArray = array();
        $i = 0;
        foreach ($resultsArray as $r_key => $r_array) {
            $formattedDebtProjectionRecord = array();
            $formattedDebtProjectionRecord['pidms'] = $qr['goremal_pidm'];

            $returnArray[$r] = $formattedEmailRecord;
            $i++;
        }
        return $returnArray;
    }
}
