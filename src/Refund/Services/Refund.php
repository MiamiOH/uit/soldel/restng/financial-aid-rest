<?php

/*
-----------------------------------------------------------
FILE NAME: Refund.class.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Erin Mills

DESCRIPTION:  The refund service is designed for get only (other functions can be added later)

INPUT:
PARAMETERS: pidm and uniqueId

ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

TABLE USAGE:

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

07/21/2015       MILLSE
Description:  Initial Program


 */

namespace MiamiOH\FinancialAidRest\Refund\Services;

class Refund extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD'; // secure datasource

    /*
    * Helper function to Set the Data Source to be used called by the frame work
    *
    * Inputs:
    * datasource: Name of Data Source to use.
    */
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    /*
    * Helper function to Set the Database source to be Used
    *
    * Inputs:
    * database: Name of database source to use.
    */
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    /*
    * Helper function to Set the Configuration to be Used
    *
    * Inputs:
    * configuration: Name of Configuration source to use.
    */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /*
     * Get Award Information (GET)
     *
     * Parameters (URL):
     * token: Authentication Token (WebServices/StudentFinancialAid view Access Required)
     * pidms: PIDMs and/or Uniqueids whose awards are to be to Retrived
     *
     * Return: Returns an array of all awards of the users requested.  Does not check for
     * 			duplicates across the pidm/uniqueid lists. This way the user gets back 
     * 			the users they request. No matter what.
     */
    public function getRefund()
    {
        //log
        $this->log->debug('Start the refund service.');

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();
        $pidms = array();
        $uniqueids = array();
        $bannerids = array();

        //check to be sure a pidm or uniqueid was added
        if (!$options ||
            ((!isset($options['pidm']) || !is_array($options['pidm']) || count($options['pidm']) == 0 || (count($options['pidm']) == 1 && $options['pidm'][0] == ""))
                && (!isset($options['uniqueid']) || !is_array($options['uniqueid']) || count($options['uniqueid']) == 0 || (count($options['uniqueid']) == 1 && $options['uniqueid'][0] == ""))
                && (!isset($options['bannerid']) || !is_array($options['bannerid']) || count($options['bannerid']) == 0 || (count($options['bannerid']) == 1 && $options['bannerid'][0] == ""))
            )) {
            //throw an error if no options were given or if the pidm and uniqueid options were both undefined
            throw new \Exception('Error: At least one pidm, one uniqueid, or one bannerid must be specified.');
        }

        //keep track of how many person paramters were sent
        $numberOfPersonParams = 0;

        //check for and get the pidm option
        if ($options && isset($options['pidm']) && count($options['pidm']) > 0) {
            $pidmParam = $options['pidm'];
            $numberOfPersonParams++;
        }

        //check for and get the uniqueid option
        if ($options && isset($options['uniqueid']) && count($options['uniqueid']) > 0) {
            $uniqueidParam = $this->upperCaseUniqueids($options['uniqueid']);

            $numberOfPersonParams++;
        }

        //check for and get the bannerid option
        if ($options && isset($options['bannerid']) && count($options['bannerid']) > 0) {
            $banneridParam = $options['bannerid'];
            $numberOfPersonParams++;
        }

        //only allow one filter for person: uniqueid, bannerid or pidm
        if ($numberOfPersonParams != 1) {
            throw new \Exception("Error: Only one of the follow parameters may be submitted at a time - uniqueid, bannerid and pidm.");
        }

        //Get Refunds for any given pidms and add to the payload
        if (isset($pidmParam) && count($pidmParam) > 0) {
            $pidmRecords = $this->buildGetPayloadForPidms($this->getRefundDataWithPidms($pidmParam), $pidmParam);
            foreach ($pidmRecords as $p => $v) { //add each array to the payload
                $payload[$p] = $v;
            }
        }

        //Get Refunds for any given uniqueids and add to the payload
        if (isset($uniqueidParam) && count($uniqueidParam) > 0) {
            if (count($options['uniqueid']) > 0 && preg_match('/^[[:alnum:]]{3,}$/', implode('', $options['uniqueid'])) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Unique IDs must only contain Numbers and Letters.');
            }
            $uniqueidRecords = $this->buildGetPayloadForUniqueids($this->getRefundDataWithUniqueids($uniqueidParam), $uniqueidParam);
            foreach ($uniqueidRecords as $u => $v) { //add each array to the payload
                $payload[$u] = $v;
            }
        }

        //Get Awards for any given bannerids and add to the payload
        if (isset($banneridParam) && count($banneridParam) > 0) {
            if (count($options['bannerid']) > 0 && preg_match('/^\\d{8}( \\d{8})*$/', implode(' ', $options['bannerid'])) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Banner ID must be 8 digits.');
            }
            $banneridRecords = $this->buildGetPayloadForBannerids($this->getRefundDataWithBannerids($banneridParam), $banneridParam);
            foreach ($banneridRecords as $u => $v) { //add each array to the payload
                $payload[$u] = $v;
            }
        }

        //Response was successful and Return information
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;

    }

    /*
     * Helper Method to Get Refund Information for PIDMs
     *
     * Parameters:
     * pidms: PIDMs to be Retrived
     *
     * Return: Returns an array of all refunds of the PIDMs requested. 
     */
    private function getRefundDataWithPidms($pidms)
    {
        try {
            //proceed if we were given pidms to work with
            if ($pidms) {
                $dbh = $this->database->getHandle($this->datasource_name);
                $queryString = "
					select
						tbraccd_pidm,
						szbuniq_unique_id,
						szbuniq_banner_id,
						tbraccd_term_code,
						tbraccd_amount
					from
						taismgr.tbraccd
						inner join szbuniq on szbuniq_pidm = tbraccd_pidm
					where tbraccd_detail_code = 'CGEN'
						and tbraccd_pidm in (
				";

                //loop through and add each pidm to the in statement if they are numeric
                foreach ($pidms as $pidm) {
                    if (is_numeric($pidm)) {
                        $queryString .= $pidm . ",";
                    } else {
                        throw new \Exception('Pidms must be numeric: ' . $pidm);
                    }
                }

                //remove the extra comma at the end of the pidms
                $queryString = substr($queryString, 0, -1);
                //add the ending paren
                $queryString .= ')';

                //execute
                $results = $dbh->queryall_array($queryString);


            } else {
                throw new \Exception('Pidm(s) are required to get refunds.');
            }

        } catch (\Exception $e) {
            throw new \Exception('Error getting data: ' . $e);
        }

        return $results;
    }

    /*
    * Helper Method to Get Refune Information for Uniqueids
    *
    * Parameters:
    * pidms: Uniqueids to be Retrived
    *
    * Return: Returns an array of all refunds of the Uniqueids requested. 
    */
    private function getRefundDataWithUniqueIds($uniqueids)
    {
        try {
            //proceed if we were given uniqueids to work with
            if ($uniqueids) {
                $dbh = $this->database->getHandle($this->datasource_name);
                $queryString = "
					select
						tbraccd_pidm,
						szbuniq_unique_id,
						szbuniq_banner_id,
						tbraccd_term_code,
						tbraccd_amount
					from
						taismgr.tbraccd
						inner join szbuniq on szbuniq_pidm = tbraccd_pidm
					where tbraccd_detail_code = 'CGEN'
						and szbuniq_unique_id in (
				";

                //loop through and add each uniqueid to the in statement
                foreach ($uniqueids as $u) {
                    $queryString .= "'" . $u . "',";
                }

                //remove the extra comma at the end of the pidms
                $queryString = substr($queryString, 0, -1);
                //add the ending paren
                $queryString .= ')';


                //execute the query
                $results = $dbh->queryall_array($queryString);

            } else {
                throw new \Exception('Uniqueids(s) are required to get refunds.');
            }

        } catch (\Exception $e) {
            throw new \Exception('Error getting data: ' . $e);
        }

        return $results;
    }

    /*
    * Helper Method to Get Refund Information for BannerIds
    *
    * Parameters:
    * pidms: Bannerids to be Retrived
    *
    * Return: Returns an array of all refunds of the Bannerids requested. 
    */
    private function getRefundDataWithBannerIds($bannerids)
    {
        try {
            //proceed if we were given uniqueids to work with
            if ($bannerids) {
                $dbh = $this->database->getHandle($this->datasource_name);
                $queryString = "
					select
						tbraccd_pidm,
						szbuniq_banner_id,
						szbuniq_unique_id,
						tbraccd_term_code,
						tbraccd_amount
					from
						taismgr.tbraccd
						inner join szbuniq on szbuniq_pidm = tbraccd_pidm
					where tbraccd_detail_code = 'CGEN'
						and szbuniq_banner_id in (
				";

                //loop through and add each uniqueid to the in statement
                foreach ($bannerids as $u) {
                    $queryString .= "'+" . $u . "',";
                }

                //remove the extra comma at the end of the pidms
                $queryString = substr($queryString, 0, -1);
                //add the ending paren
                $queryString .= ')';


                //execute the query
                $results = $dbh->queryall_array($queryString);

            } else {
                throw new \Exception('Bannerids(s) are required to get refunds.');
            }

        } catch (\Exception $e) {
            throw new \Exception('Error getting data: ' . $e);
        }

        return $results;
    }

    /*
     * Helper to build return payload for pidms.
     *
     * Parameters:
     * refundData: Associtive array of address data to process.
     * pidms: array of pidms
     *
     * Return: Associtive Array of data that should be returned.
     */
    private function buildGetPayloadForPidms($refundData, $pidms)
    {
        $records = array();

        //initialize a sub-array for each pidm given so that if they had no refunds,
        //we still get a node for that pidm anyway
        foreach ($pidms as $pidm) {
            $records[$pidm] = array();
        }

        $prettyRefundData = $this->refundMakeover($refundData);
        foreach ($prettyRefundData as $refund) {
            $records[$refund['pidm']][$this->getAidYear($refund['termCode'])][$refund['termCode']][] = array(
                'aidYear' => $refund['aidYear'],
                'termCode' => $refund['termCode'],
                'refundAmount' => $refund['refundAmount'],
            );
        }
        return $records;
    }

    /*
    * Helper to build return payload for uniqueids.
    *
    * Parameters:
    * awardData: Associtive array of refund data to process.
    * uniqueids: array of uniqueids
    *
    * Return: Associtive Array of data that should be returned.
    */
    private function buildGetPayloadForUniqueids($refundData, $uniqueids)
    {
        $records = array();

        //initialize a sub-array for each uniqueid given so that if they had no refunds,
        //we still get a node for that pidm anyway
        foreach ($uniqueids as $u) {
            $records[$u] = array();
        }

        $prettyRefundData = $this->refundMakeover($refundData);
        foreach ($prettyRefundData as $refund) {
            $records[$refund['uniqueid']][$this->getAidYear($refund['termCode'])][$refund['termCode']][] = array(
                'aidYear' => $refund['aidYear'],
                'termCode' => $refund['termCode'],
                'refundAmount' => $refund['refundAmount'],
            );
        }
        return $records;
    }

    /*
    * Helper to build return payload for bannerids.
    *
    * Parameters:
    * awardData: Associtive array of refund data to process.
    * uniqueids: array of bannerids
    *
    * Return: Associtive Array of data that should be returned.
    */
    private function buildGetPayloadForBannerids($refundData, $bannerids)
    {
        $records = array();

        //initialize a sub-array for each uniqueid given so that if they had no refunds,
        //we still get a node for that pidm anyway
        foreach ($bannerids as $u) {
            $records[$u] = array();
        }

        $prettyRefundData = $this->refundMakeover($refundData);
        foreach ($prettyRefundData as $refund) {
            $records[$refund['bannerid']][$this->getAidYear($refund['termCode'])][$refund['termCode']][] = array(
                'aidYear' => $refund['aidYear'],
                'termCode' => $refund['termCode'],
                'refundAmount' => $refund['refundAmount'],
            );
        }
        return $records;
    }

    /*
    * Helper method to turn ugly refundData into pretty refundData.
    * So beautiful.  The most prettiest data ever.
    */
    private function refundMakeover($refundData)
    {
        $prettyArray = array();
        foreach ($refundData as $refund) {
            $prettyRefund = array();
            $prettyRefund['pidm'] = $refund['tbraccd_pidm'];
            $prettyRefund['uniqueid'] = $refund['szbuniq_unique_id'];
            $prettyRefund['bannerid'] = substr($refund['szbuniq_banner_id'], 1);
            $prettyRefund['aidYear'] = $this->getAidYear($refund['tbraccd_term_code']);
            $prettyRefund['termCode'] = $refund['tbraccd_term_code'];
            $prettyRefund['refundAmount'] = $refund['tbraccd_amount'];
            $prettyArray[] = $prettyRefund;
        }
        return $prettyArray;
    }

    /*
    * Helper method to convert uniqueids to upper for safety.
    */
    private function upperCaseUniqueids($uniqueids)
    {
        $upperUniqueids = array();
        foreach ($uniqueids as $u) {
            $upperUniqueids[] = strtoupper($u);
        }
        return $upperUniqueids;
    }

    /*
    * Helper method to get the aid year from the termCode.
    */
    private function getAidYear($termCode)
    {
        // Financial aid years are always run from the one year prior to the current 
        // year to the current year. So if the year today is 2016 the aid year would be 
        // 2015-2016. 
        $year = substr($termCode, 0, 4);
        $trimYearA = substr(($year - 1), 2, 2);
        $trimYearB = substr(($year), 2, 2);
        $aidYear = $trimYearA . $trimYearB;
        return $aidYear;
    }
}