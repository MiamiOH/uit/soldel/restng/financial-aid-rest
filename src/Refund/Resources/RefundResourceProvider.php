<?php

namespace MiamiOH\FinancialAidRest\Refund\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class RefundResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Refund.TermCode',
            'type' => 'object',
            'properties' => array(
                'aidYear' => array(
                    'type' => 'number',
                ),
                'termCode' => array(
                    'type' => 'number',
                ),
                'refundAmount' => array(
                    'type' => 'number',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Refund.TermCode.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Student.FinancialAid.Refund.TermCode'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Refund.TermCodeElement',
            'type' => 'object',
            'properties' => array(
                'termCodeElement' => array(
                    'type' => 'array',
                    '$ref' => '#/definitions/Student.FinancialAid.Refund.TermCode.Collection',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Refund.TermCodeElement.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Student.FinancialAid.Refund.TermCodeElement'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Refund',
            'type' => 'object',
            'properties' => array(
                'aidYearElement' => array(
                    'type' => 'array',
                    '$ref' => '#/definitions/Student.FinancialAid.Refund.TermCodeElement.Collection',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Student.FinancialAid.Refund.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Student.FinancialAid.Refund'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(

            'name' => 'Refund',
            'class' => 'MiamiOH\FinancialAidRest\Refund\Services\Refund',
            'description' => 'Information about refunds',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'dataSource' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(
            array(
                'action' => 'read',
                'name' => 'student.financialAid.refund.get',
                'description' => "Read a student's refunds",
                'pattern' => '/student/financialAid/refund/v1',
                'service' => 'Refund',
                'method' => 'getRefund',
                'isPageable' => false,
                'tags' => array('Student'),
                'returnType' => 'collection',
                'options' => array(
                    'pidm' => array('type' => 'list', 'required' => false, 'description' => 'PIDM(s) to get debt projection records. Cannot be used in combination with uniqueid or bannerid parameters.'),
                    'uniqueid' => array('type' => 'list', 'required' => false, 'description' => 'Unique ID(s) to get debt projection records. Cannot be used in combination with pidm or bannerid parameters.'),
                    'bannerid' => array('type' => 'list', 'required' => false, 'description' => 'Banner ID(s) to get debt projection records. Cannot be used in combination with pidm or uniqueid parameters.'),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array('type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'StudentFinancialAid',
                            'key' => array('view')
                        ),
                    ),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'A collection of refund records',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/Student.FinancialAid.Refund.Collection',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}