<?php
/*
-----------------------------------------------------------
FILE NAME: getAwardBannerIDTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Axhay Patel

DESCRIPTION:  Unit Tests for Testing the GET Functionality of the Award Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE    PRJ-TSK			UniqueID
Description:

04/02/2016        PATELAH
Description:  Initial Program

*/

namespace MiamiOH\FinancialAidRest\Tests\Unit\Award;

use MiamiOH\RESTng\App;

class GetAwardBannerIDTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $award, $queryallRecords, $user, $request;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        //set up the mock api:
        $api = $this->createMock(App::class);

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        //set up the mock database:
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock datasource:
        /*$ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->setMethods(array('getDataSource'))
            ->getMock();*/

        //set up the service with the mocked out resources:
        $this->award = new \MiamiOH\FinancialAidRest\Award\Services\Award();
        $this->award->setApp($api);
        $this->award->setApiUser($this->user);
        $this->award->setDatabase($db);
        //$this->award->setDatasource($ds);
        $this->award->setRequest($this->request);

    }


    /*************************/
    /**********Tests**********/
    /*************************/

    /*
    *   Empty BannerID Given
    * 	Tests Case in which empty parameters given at all.
    *	  Expected Return: 400 Error
    */
    public function testEmptyBannerID()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockEmptyOptions')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'queryall_arrayAddressInfoSingleBannerID')));

        try {
            $resp = $this->award->getAward();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedEmptyBannerIDResult(), $e->getMessage());
        }
    }

    /*
    *	  Single BannerID Test
    * 	  Expected Return: Results seen in the mockSingleBannerIDReturn.
    */

    public function testSingleBannerID()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSingleBannerID')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryAllSingleBannerID')));

        $resp = $this->award->getAward();

        $payload = $resp->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 1);
        $this->assertEquals($payload, $this->mockSingleBannerIDReturn());

    }

    /*
    *	Multiple BannerID Test with Payment Calculation Given
    */
    public function testMultipleBannerID()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsMultipleBannerID')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryAllMultipleBannerID')));

        $resp = $this->award->getAward();
        $payload = $resp->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 2);

        $this->assertEquals($payload, $this->mockMultipleBannerIDReturn());
    }

    /*
    * Start of Mock methods
    */

    public function mockOptionsSingleBannerID()
    {
        $optionsArray = array('bannerid' => array('99999999'));
        return $optionsArray;
    }

    /*
    *	Mock the results from a successful call on a single banner ID which contains two
    *	award elements.
    */
    public function mockSingleBannerIDReturn()
    {
        $expectedReturn = array(
            '99999999' => array( //banner ID
                '0910' => array( //aid year
                    'OCGM' => array( //fund code
                        'uniqueid' => 'TESTUSER',
                        'bannerid' => '99999999',
                        'pidm' => '9999999',
                        'aidYearCode' => '0910',
                        'fundCode' => 'OCGM',
                        'fundTitle' => 'MU Coll. Op. Grant Supplement',
                        'fsrcCode' => 'INST',
                        'fsrcDescription' => 'Institutional',
                        'fsrcIndicator' => 'I',
                        'fundTypeCode' => 'SCHN',
                        'fundTypeDescription' => 'Need Based Scholarship',
                        'awstCode' => 'ACPT',
                        'awstDescription' => 'Accepted',
                        'awstDate' => '27-MAY-09',
                        'systemIndicator' => 'M',
                        'activityDate' => '01-JAN-10',
                        'lockIndicator' => 'N',
                        'offerExpirationDate' => '27-MAY-09',
                        'acceptAmount' => '2496',
                        'acceptDate' => '27-MAY-09',
                        'authorizeAmount' => '2496',
                        'authorizeDate' => '27-MAY-09',
                        'memoAmount' => '2496',
                        'memoDate' => '27-MAY-09',
                        'paidAmount' => '2496',
                        'paidDate' => '27-MAY-09',
                        'originalOfferAmount' => '2496',
                        'originalOfferDate' => '27-MAY-09',
                        'offerAmount' => '2496',
                        'offerDate' => '27-MAY-09',
                        'declineAmount' => '2496',
                        'declineDate' => '27-MAY-09',
                        'cancelAmount' => '2496',
                        'cancelDate' => '27-MAY-09'
                    ),
                ), //End of 0910
                '0708' => array( //aid year
                    'PELL' => array( //fund code
                        'uniqueid' => 'TESTUSER',
                        'bannerid' => '99999999',
                        'pidm' => '9999999',
                        'aidYearCode' => '0708',
                        'fundCode' => 'PELL',
                        'fundTitle' => 'Federal Pell Grant',
                        'fsrcCode' => 'FDRL',
                        'fsrcDescription' => 'Federal',
                        'fsrcIndicator' => 'F',
                        'fundTypeCode' => 'GRNT',
                        'fundTypeDescription' => 'Grant',
                        'awstCode' => 'ACPT',
                        'awstDescription' => 'Accepted',
                        'awstDate' => '27-MAY-09',
                        'systemIndicator' => 'S',
                        'activityDate' => '01-JAN-10',
                        'lockIndicator' => 'N',
                        'offerExpirationDate' => '27-MAY-09',
                        'acceptAmount' => '2496',
                        'acceptDate' => '27-MAY-09',
                        'authorizeAmount' => '2496',
                        'authorizeDate' => '27-MAY-09',
                        'memoAmount' => '2496',
                        'memoDate' => '27-MAY-09',
                        'paidAmount' => '2496',
                        'paidDate' => '27-MAY-09',
                        'originalOfferAmount' => '2496',
                        'originalOfferDate' => '27-MAY-09',
                        'offerAmount' => '2496',
                        'offerDate' => '27-MAY-09',
                        'declineAmount' => '2496',
                        'declineDate' => '27-MAY-09',
                        'cancelAmount' => '2496',
                        'cancelDate' => '27-MAY-09'
                    ), //End of PELL element
                ), //End of 0708 element
            ), //End of 99999999 element
        );

        return $expectedReturn;
    }

    /*
    *	Mock the expected results when for the queryall_array method for GET when a
    *	single banner ID is requested.
    */
    public function mockQueryAllSingleBannerID()
    {
        $this->queryallRecords = array(

            array(
                'szbuniq_unique_id' => 'TESTUSER',
                'szbuniq_banner_id' => '+99999999',
                'rprawrd_pidm' => '9999999',
                'rprawrd_aidy_code' => '0910',
                'rprawrd_fund_code' => 'OCGM',
                'rfrbase_fund_title' => 'MU Coll. Op. Grant Supplement',
                'rfrbase_fsrc_code' => 'INST',
                'rtvfsrc_desc' => 'Institutional',
                'rtvfsrc_ind' => 'I',
                'rfrbase_ftyp_code' => 'SCHN',
                'rtvftyp_desc' => 'Need Based Scholarship',
                'rprawrd_awst_code' => 'ACPT',
                'rtvawst_desc' => 'Accepted',
                'rprawrd_awst_date' => '27-MAY-09',
                'rprawrd_sys_ind' => 'M',
                'rprawrd_activity_date' => '01-JAN-10',
                'rprawrd_lock_ind' => 'N',
                'rprawrd_offer_exp_date' => '27-MAY-09',
                'rprawrd_accept_amt' => '2496',
                'rprawrd_accept_date' => '27-MAY-09',
                'rprawrd_authorize_amt' => '2496',
                'rprawrd_authorize_date' => '27-MAY-09',
                'rprawrd_memo_amt' => '2496',
                'rprawrd_memo_date' => '27-MAY-09',
                'rprawrd_paid_amt' => '2496',
                'rprawrd_paid_date' => '27-MAY-09',
                'rprawrd_orig_offer_amt' => '2496',
                'rprawrd_orig_offer_date' => '27-MAY-09',
                'rprawrd_offer_amt' => '2496',
                'rprawrd_offer_date' => '27-MAY-09',
                'rprawrd_decline_amt' => '2496',
                'rprawrd_decline_date' => '27-MAY-09',
                'rprawrd_cancel_amt' => '2496',
                'rprawrd_cancel_date' => '27-MAY-09'
            ),
            array(
                'szbuniq_unique_id' => 'TESTUSER',
                'szbuniq_banner_id' => '+99999999',
                'rprawrd_pidm' => '9999999',
                'rprawrd_aidy_code' => '0708',
                'rprawrd_fund_code' => 'PELL',
                'rfrbase_fund_title' => 'Federal Pell Grant',
                'rfrbase_fsrc_code' => 'FDRL',
                'rtvfsrc_desc' => 'Federal',
                'rtvfsrc_ind' => 'F',
                'rfrbase_ftyp_code' => 'GRNT',
                'rtvftyp_desc' => 'Grant',
                'rprawrd_awst_code' => 'ACPT',
                'rtvawst_desc' => 'Accepted',
                'rprawrd_awst_date' => '27-MAY-09',
                'rprawrd_sys_ind' => 'S',
                'rprawrd_activity_date' => '01-JAN-10',
                'rprawrd_lock_ind' => 'N',
                'rprawrd_offer_exp_date' => '27-MAY-09',
                'rprawrd_accept_amt' => '2496',
                'rprawrd_accept_date' => '27-MAY-09',
                'rprawrd_authorize_amt' => '2496',
                'rprawrd_authorize_date' => '27-MAY-09',
                'rprawrd_memo_amt' => '2496',
                'rprawrd_memo_date' => '27-MAY-09',
                'rprawrd_paid_amt' => '2496',
                'rprawrd_paid_date' => '27-MAY-09',
                'rprawrd_orig_offer_amt' => '2496',
                'rprawrd_orig_offer_date' => '27-MAY-09',
                'rprawrd_offer_amt' => '2496',
                'rprawrd_offer_date' => '27-MAY-09',
                'rprawrd_decline_amt' => '2496',
                'rprawrd_decline_date' => '27-MAY-09',
                'rprawrd_cancel_amt' => '2496',
                'rprawrd_cancel_date' => '27-MAY-09'

            ),
        );
        return $this->queryallRecords;
    }

    public function mockOptionsMultipleBannerID()
    {
        $optionsArray = array('bannerid' => array('99999999', '01234567'));
        return $optionsArray;
    }

    /*
   * Mock the results from a successful call on a multiple PIDM which contains two
   * award
   * Mock the results from a successful call on a multiple banner id  which contains two
   * award elements.
   */
    public function mockMultipleBannerIDReturn()
    {
        $expectedReturn = array(
            '99999999' => array( //banner ID
                '0910' => array( //aid year
                    'OCGM' => array( //fund code
                        'uniqueid' => 'TESTUSER',
                        'bannerid' => '99999999',
                        'pidm' => '9999999',
                        'aidYearCode' => '0910',
                        'fundCode' => 'OCGM',
                        'fundTitle' => 'MU Coll. Op. Grant Supplement',
                        'fsrcCode' => 'INST',
                        'fsrcDescription' => 'Institutional',
                        'fsrcIndicator' => 'I',
                        'fundTypeCode' => 'SCHN',
                        'fundTypeDescription' => 'Need Based Scholarship',
                        'awstCode' => 'ACPT',
                        'awstDescription' => 'Accepted',
                        'awstDate' => '27-MAY-09',
                        'systemIndicator' => 'M',
                        'activityDate' => '01-JAN-10',
                        'lockIndicator' => 'N',
                        'offerExpirationDate' => '27-MAY-09',
                        'acceptAmount' => '2496',
                        'acceptDate' => '27-MAY-09',
                        'authorizeAmount' => '2496',
                        'authorizeDate' => '27-MAY-09',
                        'memoAmount' => '2496',
                        'memoDate' => '27-MAY-09',
                        'paidAmount' => '2496',
                        'paidDate' => '27-MAY-09',
                        'originalOfferAmount' => '2496',
                        'originalOfferDate' => '27-MAY-09',
                        'offerAmount' => '2496',
                        'offerDate' => '27-MAY-09',
                        'declineAmount' => '2496',
                        'declineDate' => '27-MAY-09',
                        'cancelAmount' => '2496',
                        'cancelDate' => '27-MAY-09'
                    ), //End of OCGM
                ), //End of 0910
            ), //End of 99999999
            '01234567' => array( //banner id
                '0708' => array( //aid year
                    'PELL' => array( //fund code
                        'uniqueid' => 'TESTUSER',
                        'bannerid' => '01234567',
                        'pidm' => '9999999',
                        'aidYearCode' => '0708',
                        'fundCode' => 'PELL',
                        'fundTitle' => 'Federal Pell Grant',
                        'fsrcCode' => 'FDRL',
                        'fsrcDescription' => 'Federal',
                        'fsrcIndicator' => 'F',
                        'fundTypeCode' => 'GRNT',
                        'fundTypeDescription' => 'Grant',
                        'awstCode' => 'ACPT',
                        'awstDescription' => 'Accepted',
                        'awstDate' => '27-MAY-09',
                        'systemIndicator' => 'S',
                        'activityDate' => '01-JAN-10',
                        'lockIndicator' => 'N',
                        'offerExpirationDate' => '27-MAY-09',
                        'acceptAmount' => '2496',
                        'acceptDate' => '27-MAY-09',
                        'authorizeAmount' => '2496',
                        'authorizeDate' => '27-MAY-09',
                        'memoAmount' => '2496',
                        'memoDate' => '27-MAY-09',
                        'paidAmount' => '2496',
                        'paidDate' => '27-MAY-09',
                        'originalOfferAmount' => '2496',
                        'originalOfferDate' => '27-MAY-09',
                        'offerAmount' => '2496',
                        'offerDate' => '27-MAY-09',
                        'declineAmount' => '2496',
                        'declineDate' => '27-MAY-09',
                        'cancelAmount' => '2496',
                        'cancelDate' => '27-MAY-09'
                    ), //End of PELL element
                ), //End of 0708 element
            ) //End of 1234567 element

        );
        return $expectedReturn;
    }

    /*
    *	Mock the expected results when for the queryall_array method for GET when a
    *	multiple banner ID is requested. Consists of two rows.
    */
    public function mockQueryAllMultipleBannerID()
    {
        $this->queryallRecords = array(
            array( //99999999
                'szbuniq_unique_id' => 'TESTUSER',
                'szbuniq_banner_id' => '+99999999',
                'rprawrd_pidm' => '9999999',
                'rprawrd_aidy_code' => '0910',
                'rprawrd_fund_code' => 'OCGM',
                'rfrbase_fund_title' => 'MU Coll. Op. Grant Supplement',
                'rfrbase_fsrc_code' => 'INST',
                'rtvfsrc_desc' => 'Institutional',
                'rtvfsrc_ind' => 'I',
                'rfrbase_ftyp_code' => 'SCHN',
                'rtvftyp_desc' => 'Need Based Scholarship',
                'rprawrd_awst_code' => 'ACPT',
                'rtvawst_desc' => 'Accepted',
                'rprawrd_awst_date' => '27-MAY-09',
                'rprawrd_sys_ind' => 'M',
                'rprawrd_activity_date' => '01-JAN-10',
                'rprawrd_lock_ind' => 'N',
                'rprawrd_offer_exp_date' => '27-MAY-09',
                'rprawrd_accept_amt' => '2496',
                'rprawrd_accept_date' => '27-MAY-09',
                'rprawrd_authorize_amt' => '2496',
                'rprawrd_authorize_date' => '27-MAY-09',
                'rprawrd_memo_amt' => '2496',
                'rprawrd_memo_date' => '27-MAY-09',
                'rprawrd_paid_amt' => '2496',
                'rprawrd_paid_date' => '27-MAY-09',
                'rprawrd_orig_offer_amt' => '2496',
                'rprawrd_orig_offer_date' => '27-MAY-09',
                'rprawrd_offer_amt' => '2496',
                'rprawrd_offer_date' => '27-MAY-09',
                'rprawrd_decline_amt' => '2496',
                'rprawrd_decline_date' => '27-MAY-09',
                'rprawrd_cancel_amt' => '2496',
                'rprawrd_cancel_date' => '27-MAY-09',
            ),
            array(
                'szbuniq_unique_id' => 'TESTUSER',
                'szbuniq_banner_id' => '+01234567',
                'rprawrd_pidm' => '9999999',
                'rprawrd_aidy_code' => '0708',
                'rprawrd_fund_code' => 'PELL',
                'rfrbase_fund_title' => 'Federal Pell Grant',
                'rfrbase_fsrc_code' => 'FDRL',
                'rtvfsrc_desc' => 'Federal',
                'rtvfsrc_ind' => 'F',
                'rfrbase_ftyp_code' => 'GRNT',
                'rtvftyp_desc' => 'Grant',
                'rprawrd_awst_code' => 'ACPT',
                'rtvawst_desc' => 'Accepted',
                'rprawrd_awst_date' => '27-MAY-09',
                'rprawrd_sys_ind' => 'S',
                'rprawrd_activity_date' => '01-JAN-10',
                'rprawrd_lock_ind' => 'N',
                'rprawrd_offer_exp_date' => '27-MAY-09',
                'rprawrd_accept_amt' => '2496',
                'rprawrd_accept_date' => '27-MAY-09',
                'rprawrd_authorize_amt' => '2496',
                'rprawrd_authorize_date' => '27-MAY-09',
                'rprawrd_memo_amt' => '2496',
                'rprawrd_memo_date' => '27-MAY-09',
                'rprawrd_paid_amt' => '2496',
                'rprawrd_paid_date' => '27-MAY-09',
                'rprawrd_orig_offer_amt' => '2496',
                'rprawrd_orig_offer_date' => '27-MAY-09',
                'rprawrd_offer_amt' => '2496',
                'rprawrd_offer_date' => '27-MAY-09',
                'rprawrd_decline_amt' => '2496',
                'rprawrd_decline_date' => '27-MAY-09',
                'rprawrd_cancel_amt' => '2496',
                'rprawrd_cancel_date' => '27-MAY-09',
            )
        );

        return $this->queryallRecords;
    }

    // Empty banner ID options
    public function mockEmptyOptions()
    {
        $optionsArray = array('bannerid' => array(''));
        return $optionsArray;
    }

    public function mockExpectedEmptyBannerIDResult()
    {
        return "Error: At least one pidm, one uniqueid or one bannerid must be specified.";
    }

    public function mockResourceParams()
    {
        return array();
    }

}
	   