<?php
/*
-----------------------------------------------------------
FILE NAME: awardGetSQLInjectionTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Axhay Patel

DESCRIPTION: 
This php class is used to test the GET Method for Award Web Service for SQL
Injection

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit

TABLE USAGE:

Web Service Usage:
	Student/FinancialAid/Award service (GET)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

03/24/2016               PATELAH
Description:  Initial Draft
			 
-----------------------------------------------------------
 */

namespace MiamiOH\FinancialAidRest\Tests\Unit\Award;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Request;

class AwardGetSQLInjectionTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $api, $request, $dbh, $user, $award, $queryallRecords;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->createMock(Request::class);

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

       /* $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->setMethods(array('getDataSource'))
            ->getMock();*/

        //set up the service with the mocked out resources:
        $this->award = new \MiamiOH\FinancialAidRest\Award\Services\Award();
        $this->award->setApp($this->api);
        $this->award->setApiUser($this->user);
        $this->award->setDatabase($db);
        //$this->award->setDatasource($ds);
        $this->award->setRequest($this->request);

    }

    /*************************/
    /**********Tests**********/
    /*************************/


    /*
     *	  Test for PIDM SQL Injection Attack
     * 	Tests when a SQL Injection attack is done on PIDM parameter.
     */
    public function testPidmSQLInjection()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsPidmOnlyAttack')));

        try {
            $resp = $this->award->getAward();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedMessageReturnPIDMAttack(), $e->getMessage());
        }

    }

    /*
      *	  Test for UniqueID SQL Injection Attack
      * 	Tests when a SQL Injection attack is done on UniqueID parameter.
      */
    public function testUniqueIDSQLInjection()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsUniqueIDAttack')));

        try {
            $resp = $this->award->getAward();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedMessageReturnUniqueIDAttack(), $e->getMessage());
        }
    }


    /*
       *	  Test for BannerID SQL Injection Attack
       * 	Tests when a SQL Injection attack is done on UniqueID parameter.
       */
    public function testBannerIDSQLInjection()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsBannerIDAttack')));

        try {
            $resp = $this->award->getAward();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedMessageReturnBannerIDAttack(), $e->getMessage());
        }
    }

    /*************************/
    /**Start of Mock Methods**/
    /*************************/

    public function mockAuthorizedUser()
    {
        return true;
    }

    //SQL Injection Options Mock Methods
    public function mockOptionsPidmOnlyAttack()
    {
        $optionsArray = array('pidm' => array('\';--'));
        return $optionsArray;
    }

    public function mockOptionsUniqueIDAttack()
    {
        $optionsArray = array('uniqueid' => array('\';--'));
        return $optionsArray;
    }

    public function mockOptionsBannerIDAttack()
    {
        $optionsArray = array('bannerid' => array('\';--'));
        return $optionsArray;
    }

    //SQL Injection Expected Returns
    public function mockExpectedMessageReturnPIDMAttack()
    {
        return "Pidms must be numeric.";
    }

    public function mockExpectedMessageReturnUniqueIDAttack()
    {
        return "Unique IDs must only contain Numbers and Letters.";
    }

    public function mockExpectedMessageReturnBannerIDAttack()
    {
        return "Banner ID must be 8 digits.";
    }

}