<?php

/*
-----------------------------------------------------------
FILE NAME: getDebtProjectionPIDMTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION: 
This php class is used to test the GET method of the refund service. Specifically 
UniquieID paramater usage.

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit
Student/FinancialAid/Refund

TABLE USAGE:

Web Service Usage:
	Student/FinancialAid/Refund service (GET)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

02/XX/2016               SCHMIDEE
Description:  Initial Draft
			 
-----------------------------------------------------------
 */
namespace MiamiOH\FinancialAidRest\Tests\Unit\Refund;

use MiamiOH\RESTng\App;

class GetDebtProjectionUniqueIDTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $refund, $queryallRecords, $user, $request, $awardService, $api;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        /*$ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->setMethods(array('getDataSource'))
            ->getMock();*/

        //set up the service with the mocked out resources:
        $this->refund = new \MiamiOH\FinancialAidRest\Refund\Services\Refund();
        $this->refund->setApp($this->api);
        $this->refund->setApiUser($this->user);
        $this->refund->setDatabase($db);
        //$this->refund->setDatasource($ds);
        $this->refund->setRequest($this->request);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /*
     *	Invalid User Test
     * 	Tests Case in which a user is not Authorized to use this service.
     *	Expected Return: 401 Forbidden Error
     *
     public function testInvalidAuthorization() {
         //tell the dbh what to do when the queryall_array method is called.
         $this->user->method('isAuthorized')
         ->will($this->returnCallback(array($this, 'mockNotAuthorizedUser')));
         $resp    = $this->debtProjection->getDebtProjection();

         //get the response and payload from the getSchedule() method.
            $this->assertEquals(MiamiOH\RESTng\App::API_UNAUTHORIZED, $resp->getStatus());
     }
     */

    /*
     *   Empty PIDM Given
     * 	Tests Case in which empty parameteres given at aall.
     *	Expected Return: 400 Error
     */
    public function testEmptyUniqueID()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockEmptyParameters')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'queryall_arrayAddressInfoSingleUniqueID')));

        try {
            $resp = $this->refund->getRefund();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedEmptyUniqueIDResult(), $e->getMessage());
        }

    }


    /*
     *	Single PIDM Test
     * 	Tests when a single PIDM is requested.
     *	Expected Return: Results seen in the mockSingleUniqueIDResults.
     */
    public function testSingleUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSingleUniqueID')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryAllSingleUniqueID')));


        $resp = $this->refund->getRefund();

        $payload = $resp->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 1);
        $this->assertEquals($payload, $this->mockSingleUniqueIDResults());

    }

    /*
     *	Multiple PIDM
     * 	Tests when  multiple PIDMs are requested.
     *	Expected Return: Results seen in the mockMultipleUniqueIDResults.
     */
    public function testMultipleUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsMultipleUniqueID')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryAllMultipleUniqueID')));

        $resp = $this->refund->getRefund();
        $payload = $resp->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 4);
        $this->assertEquals($payload, $this->mockMultipleUniqueIDResults());


    }

    /*************************/
    /**Start of Mock Methods**/
    /*************************/

    public function mockAuthorizedUser()
    {
        return true;
    }

    //Payment Calculation Returns and Parameters Mock Methods
    public function mockOptionsSingleUniqueID()
    {
        $optionsArray = array('uniqueid' => array('testuser'));
        return $optionsArray;
    }

    public function mockSingleUniqueIDResults()
    {
        $returnArray = array(
            'TESTUSER' => array(
                "1213" => array(
                    "201320" => array(
                        array(
                            "aidYear" => "1213",
                            "termCode" => "201320",
                            "refundAmount" => "1.0"),
                        array(
                            "aidYear" => "1213",
                            "termCode" => "201320",
                            "refundAmount" => "3.0")
                    ),
                ),    //End of 1213 Element
                "1314" => array(
                    "201410" => array(
                        array(
                            "aidYear" => "1314",
                            "termCode" => "201410",
                            "refundAmount" => "4.0"),
                    ),
                    "201420" => array(
                        array(
                            "aidYear" => "1314",
                            "termCode" => "201420",
                            "refundAmount" => "5.0"),
                    ),
                ),    //End of 1314 Element

            ), // End of 9999999 Element
        );
        return $returnArray;
    }

    public function mockQueryAllSingleUniqueID()
    {
        return array(
            array(
                'tbraccd_pidm' => '9999999',
                'szbuniq_unique_id' => 'TESTUSER',
                'szbuniq_banner_id' => '9999999',
                'tbraccd_term_code' => '201320',
                'tbraccd_amount' => '1.0'
            ),
            array(
                'tbraccd_pidm' => '9999999',
                'szbuniq_unique_id' => 'TESTUSER',
                'szbuniq_banner_id' => '9999999',
                'tbraccd_term_code' => '201320',
                'tbraccd_amount' => '3.0'
            ),
            array(
                'tbraccd_pidm' => '9999999',
                'szbuniq_unique_id' => 'TESTUSER',
                'szbuniq_banner_id' => '9999999',
                'tbraccd_term_code' => '201410',
                'tbraccd_amount' => '4.0'
            ),
            array(
                'tbraccd_pidm' => '9999999',
                'szbuniq_unique_id' => 'TESTUSER',
                'szbuniq_banner_id' => '9999999',
                'tbraccd_term_code' => '201420',
                'tbraccd_amount' => '5.0'
            ),
        );
    }

    public function mockOptionsMultipleUniqueID()
    {
        $optionsArray = array('uniqueid' => array('testuser', 'testuser1', 'testuser2', 'testuser3'));
        return $optionsArray;
    }

    public function mockMultipleUniqueIDResults()
    {
        $returnArray = array(
            'TESTUSER' => array(
                "1213" => array(
                    "201320" => array(
                        array(
                            "aidYear" => "1213",
                            "termCode" => "201320",
                            "refundAmount" => "1.0"),
                        array(
                            "aidYear" => "1213",
                            "termCode" => "201320",
                            "refundAmount" => "3.0")
                    ),
                ),    //End of 1213 Element
                "1314" => array(
                    "201410" => array(
                        array(
                            "aidYear" => "1314",
                            "termCode" => "201410",
                            "refundAmount" => "4.0"),
                    ),
                    "201420" => array(
                        array(
                            "aidYear" => "1314",
                            "termCode" => "201420",
                            "refundAmount" => "5.0"),
                    ),
                ),    //End of 1314 Element
            ),
            'TESTUSER1' => array(
                "9900" => array(
                    "200010" => array(
                        array(
                            "aidYear" => "9900",
                            "termCode" => "200010",
                            "refundAmount" => "0.0"),
                        array(
                            "aidYear" => "9900",
                            "termCode" => "200010",
                            "refundAmount" => "3.0")
                    ),
                ),    //End of 9900 Element
                "0001" => array(
                    "200120" => array(
                        array(
                            "aidYear" => "0001",
                            "termCode" => "200120",
                            "refundAmount" => "0.0"),
                        array(
                            "aidYear" => "0001",
                            "termCode" => "200120",
                            "refundAmount" => "3.0")
                    ),
                ),    //End of 0001 Element
                "0102" => array(
                    "200220" => array(
                        array(
                            "aidYear" => "0102",
                            "termCode" => "200220",
                            "refundAmount" => "0.0"),
                        array(
                            "aidYear" => "0102",
                            "termCode" => "200220",
                            "refundAmount" => "3.0")
                    ),
                ),    //End of 0102 Element
            ),
            'TESTUSER2' => array(
                "1920" => array(
                    "202020" => array(
                        array(
                            "aidYear" => "1920",
                            "termCode" => "202020",
                            "refundAmount" => "0.0"),
                        array(
                            "aidYear" => "1920",
                            "termCode" => "202020",
                            "refundAmount" => "3.0")
                    ),
                    "202030" => array(
                        array(
                            "aidYear" => "1920",
                            "termCode" => "202030",
                            "refundAmount" => "0.0"),
                        array(
                            "aidYear" => "1920",
                            "termCode" => "202030",
                            "refundAmount" => "3.0")
                    ),
                    "202010" => array(
                        array(
                            "aidYear" => "1920",
                            "termCode" => "202010",
                            "refundAmount" => "0.0"),
                        array(
                            "aidYear" => "1920",
                            "termCode" => "202010",
                            "refundAmount" => "3.0")
                    ),
                    "202015" => array(
                        array(
                            "aidYear" => "1920",
                            "termCode" => "202015",
                            "refundAmount" => "0.0"),
                        array(
                            "aidYear" => "1920",
                            "termCode" => "202015",
                            "refundAmount" => "3.0")
                    ),

                ),    //End of 2021 Element
            ),
            'TESTUSER3' => array()
        );
        return $returnArray;
    }

    public function mockQueryAllMultipleUniqueID()
    {
        return array(
            //9999999
            array(
                'tbraccd_pidm' => '9999999',
                'szbuniq_unique_id' => 'TESTUSER',
                'szbuniq_banner_id' => '9999999',
                'tbraccd_term_code' => '201320',
                'tbraccd_amount' => '1.0'
            ),
            array(
                'tbraccd_pidm' => '9999999',
                'szbuniq_unique_id' => 'TESTUSER',
                'szbuniq_banner_id' => '9999999',
                'tbraccd_term_code' => '201320',
                'tbraccd_amount' => '3.0'
            ),
            array(
                'tbraccd_pidm' => '9999999',
                'szbuniq_unique_id' => 'TESTUSER',
                'szbuniq_banner_id' => '9999999',
                'tbraccd_term_code' => '201410',
                'tbraccd_amount' => '4.0'
            ),
            array(
                'tbraccd_pidm' => '9999999',
                'szbuniq_unique_id' => 'TESTUSER',
                'szbuniq_banner_id' => '9999999',
                'tbraccd_term_code' => '201420',
                'tbraccd_amount' => '5.0'
            ),
            //1234567
            array(
                'tbraccd_pidm' => '1234567',
                'szbuniq_unique_id' => 'TESTUSER1',
                'szbuniq_banner_id' => '1234567',
                'tbraccd_term_code' => '200010',
                'tbraccd_amount' => '0.0'
            ),
            array(
                'tbraccd_pidm' => '1234567',
                'szbuniq_unique_id' => 'TESTUSER1',
                'szbuniq_banner_id' => '1234567',
                'tbraccd_term_code' => '200010',
                'tbraccd_amount' => '3.0'
            ),
            array(
                'tbraccd_pidm' => '1234567',
                'szbuniq_unique_id' => 'TESTUSER1',
                'szbuniq_banner_id' => '1234567',
                'tbraccd_term_code' => '200120',
                'tbraccd_amount' => '0.0'
            ),
            array(
                'tbraccd_pidm' => '1234567',
                'szbuniq_unique_id' => 'TESTUSER1',
                'szbuniq_banner_id' => '1234567',
                'tbraccd_term_code' => '200120',
                'tbraccd_amount' => '3.0'
            ),
            array(
                'tbraccd_pidm' => '1234567',
                'szbuniq_unique_id' => 'TESTUSER1',
                'szbuniq_banner_id' => '1234567',
                'tbraccd_term_code' => '200220',
                'tbraccd_amount' => '0.0'
            ),
            array(
                'tbraccd_pidm' => '1234567',
                'szbuniq_unique_id' => 'TESTUSER1',
                'szbuniq_banner_id' => '1234567',
                'tbraccd_term_code' => '200220',
                'tbraccd_amount' => '3.0'
            ),
            //0000001
            array(
                'tbraccd_pidm' => '0000001',
                'szbuniq_unique_id' => 'TESTUSER2',
                'szbuniq_banner_id' => '0000001',
                'tbraccd_term_code' => '202020',
                'tbraccd_amount' => '0.0'
            ),
            array(
                'tbraccd_pidm' => '0000001',
                'szbuniq_unique_id' => 'TESTUSER2',
                'szbuniq_banner_id' => '0000001',
                'tbraccd_term_code' => '202020',
                'tbraccd_amount' => '3.0'
            ),
            array(
                'tbraccd_pidm' => '0000001',
                'szbuniq_unique_id' => 'TESTUSER2',
                'szbuniq_banner_id' => '0000001',
                'tbraccd_term_code' => '202030',
                'tbraccd_amount' => '0.0'
            ),
            array(
                'tbraccd_pidm' => '0000001',
                'szbuniq_unique_id' => 'TESTUSER2',
                'szbuniq_banner_id' => '0000001',
                'tbraccd_term_code' => '202030',
                'tbraccd_amount' => '3.0'
            ),
            array(
                'tbraccd_pidm' => '0000001',
                'szbuniq_unique_id' => 'TESTUSER2',
                'szbuniq_banner_id' => '0000001',
                'tbraccd_term_code' => '202010',
                'tbraccd_amount' => '0.0'
            ),
            array(
                'tbraccd_pidm' => '0000001',
                'szbuniq_unique_id' => 'TESTUSER2',
                'szbuniq_banner_id' => '0000001',
                'tbraccd_term_code' => '202010',
                'tbraccd_amount' => '3.0'
            ),
            array(
                'tbraccd_pidm' => '0000001',
                'szbuniq_unique_id' => 'TESTUSER2',
                'szbuniq_banner_id' => '0000001',
                'tbraccd_term_code' => '202015',
                'tbraccd_amount' => '0.0'
            ),
            array(
                'tbraccd_pidm' => '0000001',
                'szbuniq_unique_id' => 'TESTUSER2',
                'szbuniq_banner_id' => '0000001',
                'tbraccd_term_code' => '202015',
                'tbraccd_amount' => '3.0'
            ),
        );
    }

    //No PIDM or Unique ID Return
    public function mockNoParameters()
    {
        $optionsArray = array();
        return $optionsArray;
    }

    //Empty PIDM or Unique ID Return
    public function mockEmptyParameters()
    {
        $optionsArray = array('uniqueid' => array(''));
        return $optionsArray;
    }

    public function mockExpectedEmptyUniqueIDResult()
    {
        return "Error: At least one pidm, one uniqueid, or one bannerid must be specified.";
    }


    public function mockResourceParams()
    {
        return array();
    }

}