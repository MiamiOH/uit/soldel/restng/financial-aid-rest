<?php

/*
-----------------------------------------------------------
FILE NAME: getRefundTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Erin Mills

DESCRIPTION: 
This php class is used to test the GET Method for Refund Web Service for SQL
Injection

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit
Student/FiniancialAid/Refund Service

TABLE USAGE:

Web Service Usage:
	Student/FinancialAid/Refund service (GET)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

02/XX/2016               SCHMIDEE
Description:  Initial Draft
			 
-----------------------------------------------------------
 */
namespace MiamiOH\FinancialAidRest\Tests\Unit\Refund;

use MiamiOH\RESTng\App;

class GetRefundSQLInjectionTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $debtProjection, $queryallRecords, $user, $request, $awardService, $api;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        /*$ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->setMethods(array('getDataSource'))
            ->getMock();*/

        //set up the service with the mocked out resources:
        $this->debtProjection = new \MiamiOH\FinancialAidRest\Refund\Services\Refund();
        $this->debtProjection->setApp($this->api);
        $this->debtProjection->setApiUser($this->user);
        $this->debtProjection->setDatabase($db);
       // $this->debtProjection->setDatasource($ds);
        $this->debtProjection->setRequest($this->request);

    }

    /*************************/
    /**********Tests**********/
    /*************************/


    /*
     *	Test for PIDM SQL Injection Attack
     * 	Tests when a SQL Injection attack is done on PIDM parameter.
     */
    public function testPidmSQLInjection()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsPidmOnlyAttack')));

        try {
            $resp = $this->debtProjection->getRefund();
        } catch (\Exception $e) {
            $this->assertContains($this->mockExpectedMessageReturnPIDMAttack(), $e->getMessage());
        }

    }

    /*
     *	Test for UniqueID SQL Injection Attack
     * 	Tests when a SQL Injection attack is done on UniqueID parameter.
     */
    public function testUniqueIDSQLInjection()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsUniqueIDAttack')));

        try {
            $resp = $this->debtProjection->getRefund();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedMessageReturnUniqueIDAttack(), $e->getMessage());
        }


    }

    /*
     *	Test for BannerID SQL Injection Attack
     * 	Tests when a SQL Injection attack is done on UniqueID parameter.
     */
    public function testBannerIDSQLInjection()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsBannerIDAttack')));

        try {
            $resp = $this->debtProjection->getRefund();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedMessageReturnBannerIDAttack(), $e->getMessage());
        }


    }

    /*************************/
    /**Start of Mock Methods**/
    /*************************/

    public function mockAuthorizedUser()
    {
        return true;
    }

    //SQL Injection Options Mock Methods
    public function mockOptionsPidmOnlyAttack()
    {
        $optionsArray = array('pidm' => array('\';--'));
        return $optionsArray;
    }


    public function mockOptionsUniqueIDAttack()
    {
        $optionsArray = array('uniqueid' => array('\';--'));
        return $optionsArray;
    }

    public function mockOptionsBannerIDAttack()
    {
        $optionsArray = array('bannerid' => array('\';--'));
        return $optionsArray;
    }

    public function mockOptionsInterestAttack()
    {
        $optionsArray = array('pidm' => array('9999999'),
            'interestRate' => '\';--',
            'numberOfYears' => 10

        );
        return $optionsArray;
    }

    public function mockOptionsNumberOfYearsAttack()
    {
        $optionsArray = array('pidm' => array('9999999'),
            'interestRate' => '10.8',
            'numberOfYears' => '\';--'

        );
        return $optionsArray;
    }

    //SQL Injection Expected Returns
    public function mockExpectedMessageReturnPIDMAttack()
    {
        return "Pidms must be numeric: ';--";
    }

    public function mockExpectedMessageReturnUniqueIDAttack()
    {
        return "Unique IDs must only contain Numbers and Letters.";
    }

    public function mockExpectedMessageReturnBannerIDAttack()
    {
        return "Banner ID must be 8 digits.";
    }

    //Mock Award Recoursce calls

    public function callResourceMock()
    {
        $response = new \MiamiOH\RESTng\Util\Response();

        $id = '';
        if (isset($this->resourceBeingCalledArgs['params']['id'])) {
            $id = $this->resourceBeingCalledArgs['params']['id'];
        } elseif (isset($this->resourceBeingCalledArgs['options']['id'])) {
            $id = $this->resourceBeingCalledArgs['options']['id'];
        } elseif (isset($this->resourceBeingCalledArgs['data']['data'][0]['uniqueid'])) {
            $id = $this->resourceBeingCalledArgs['data']['data'][0]['uniqueid'];
        }

        //print "Calling " . $this->resourceBeingCalledName . " ($id)\n";

        if (isset($this->callResourceMockResponse[$this->resourceBeingCalledName])) {
            $mockResponseData = $this->callResourceMockResponse[$this->resourceBeingCalledName];
            $response->setStatus($mockResponseData['status']);
            $response->setPayload($mockResponseData['payload']);
        }

        $this->resourceResponses[$this->resourceBeingCalledName] = array(
            'status' => $response->getStatus(),
            'payload' => $response->getPayload(),
        );

        return $response;

    }

}