<?php

/*
-----------------------------------------------------------
FILE NAME: getrefundInvalidTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION: 
This php class is used to test the GET method of the DebtProjeciton service. Specifically
invalid parameter usage. 

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit
Student/FinancialAid/Refund Service

TABLE USAGE:

Web Service Usage:
	Student/FinancialAid/Refund service (GET)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

02/XX/2016               SCHMIDEE
Description:  Initial Draft
			 
-----------------------------------------------------------
 */
namespace MiamiOH\FinancialAidRest\Tests\Unit\Refund;

use MiamiOH\RESTng\App;

class GetRefundInvalidTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $refund, $queryallRecords, $user, $request, $awardService, $api;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

       /* $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->setMethods(array('getDataSource'))
            ->getMock();*/

        //set up the service with the mocked out resources:
        $this->refund = new \MiamiOH\FinancialAidRest\Refund\Services\Refund();
        $this->refund->setApp($this->api);
        $this->refund->setApiUser($this->user);
        $this->refund->setDatabase($db);
        //$this->refund->setDatasource($ds);
        $this->refund->setRequest($this->request);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /*
     *   No UniqueID or PIDM or BannerID Given
     * 	Tests Case in which no parameteres given at aall.
     *	Expected Return: 400 Error
     */
    public function testNoParameters()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        try {
            $resp = $this->refund->getRefund();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedNoPidmUniqueIDResult(), $e->getMessage());
        }
    }


    /*
     *   Empty UniqueID and PIDM Given
     * 	Tests Case in which empty parameteres given at aall.
     *	Expected Return: 400 Error
     */
    public function testEmptyPidmUniqueID()
    {

        $this->callResourceMockResponse = array();
        //	$this->callResourceMockResponse = $this->mockCallToAwardServiceSingle();

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockEmptyParameters')));


        try {
            $resp = $this->refund->getRefund();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedInvalidSearchParameterCombo(), $e->getMessage());
        }
    }


    /*************************/
    /**Start of Mock Methods**/
    /*************************/

    public function mockAuthorizedUser()
    {
        return true;
    }

    //No PIDM or Unique ID Return
    public function mockNoParameters()
    {
        $optionsArray = array();
        return $optionsArray;
    }

    public function mockExpectedNoPidmUniqueIDResult()
    {
        return "Error: At least one pidm, one uniqueid, or one bannerid must be specified.";
    }

    public function mockExpectedInvalidSearchParameterCombo()
    {
        return "Error: Only one of the follow parameters may be submitted at a time - uniqueid, bannerid and pidm.";
    }

    //Empty PIDM or Unique ID Return
    public function mockEmptyParameters()
    {
        $optionsArray = array('pidm' => array('123'),
            'uniqueid' => array('asdf1'));
        return $optionsArray;
    }

    public function mockExpectedEmptyPidmUniqueIDResult()
    {
        return "Error: At least one pidm or one uniqueid must be specified.";
    }

}
	