<?php

/*
-----------------------------------------------------------
FILE NAME: getDebtProjectionInvalidTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION: 
This php class is used to test the GET method of the DebtProjeciton service. Specifically
invalid parameter usage. 

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit
Student/FinancialAid/DebtProjection Service

TABLE USAGE:

Web Service Usage:
	Student/FinancialAid/DebtProjection service (GET)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

02/XX/2016               SCHMIDEE
Description:  Initial Draft
			 
-----------------------------------------------------------
 */
namespace MiamiOH\FinancialAidRest\Tests\Unit\DebtProjection;

use MiamiOH\RESTng\App;

class GetDebtProjectionInvalidTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $debtProjection, $queryallRecords, $user, $request, $awardService, $api;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

       /* $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->setMethods(array('getDataSource'))
            ->getMock();*/

        //set up the service with the mocked out resources:
        $this->debtProjection = new \MiamiOH\FinancialAidRest\DebtProjection\Services\DebtProjection();
        $this->debtProjection->setApp($this->api);
        $this->debtProjection->setApiUser($this->user);
        $this->debtProjection->setDatabase($db);
       // $this->debtProjection->setDatasource($ds);
        $this->debtProjection->setRequest($this->request);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /*
     *	Invalid User Test
     * 	Tests Case in which a user is not Authorized to use this service.
     *	Expected Return: 401 Forbidden Error
     *
     public function testInvalidAuthorization() {
         //tell the dbh what to do when the queryall_array method is called.
         $this->user->method('isAuthorized')
         ->will($this->returnCallback(array($this, 'mockNotAuthorizedUser')));
         $resp    = $this->debtProjection->getDebtProjection();

         //get the response and payload from the getSchedule() method.
            $this->assertEquals(MiamiOH\RESTng\App::API_UNAUTHORIZED, $resp->getStatus());
     }
     */

    /*
     *   No UniqueID or PIDM or BannerID Given
     * 	Tests Case in which no parameteres given at aall.
     *	Expected Return: 400 Error
     */
    public function testNoParameters()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        try {
            $resp = $this->debtProjection->getDebtProjection();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedNoPidmUniqueIDResult(), $e->getMessage());
        }
    }


    /*
     *   Empty UniqueID and PIDM Given
     * 	Tests Case in which empty parameteres given at aall.
     *	Expected Return: 400 Error
     */
    public function testEmptyPidmUniqueID()
    {

        $this->callResourceMockResponse = array();
        //	$this->callResourceMockResponse = $this->mockCallToAwardServiceSingle();

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockEmptyParameters')));


        try {
            $resp = $this->debtProjection->getDebtProjection();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedInvalidSearchParameterCombo(), $e->getMessage());
        }
    }


    /*
     *	Single PIDM and UniqueID Test with No Payment Calculation Given
     * 	Tests when no payment calculation paramters are given and a single PIDM and
     *   UniqueID is requested.
     *	Expected Return: Results seen in the mockSinglePidmUniqueIDResultsNoPaymentCal.
     */
    public function testSinglePidmUniqueIDNoPaymentCal()
    {
        $this->callResourceMockResponse = array();
        //$this->callResourceMockResponse = $this->mockCallToAwardServiceSingleUniqueID();

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSinglePidmUniqueIDNoPaymentCal')));


        try {
            $resp = $this->debtProjection->getDebtProjection();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedInvalidSearchParameterCombo(), $e->getMessage());
        }

    }

    /*
     *	Multiple PIDM and UniqueID Test with No Payment Calculation Given
     * 	Tests when no payment calculation paramters are given and a multiple PIDMs and
     *   UniqueIDs are requested.
     *	Expected Return: Results seen in the mockMultiplePidmResultsNoPaymentCal.
     */
    public function testMultiplePidmUniqueIDNoPaymentCal()
    {
        //$this->callResourceMockResponse = $this->mockCallToAwardServiceMultipleUniqueID();

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsMultiplePidmUniqueIDNoPaymentCal')));

        try {
            $resp = $this->debtProjection->getDebtProjection();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedInvalidSearchParameterCombo(), $e->getMessage());
        }


    }


    /*
     *	Single PIDM and UniqueID Test with No Payment Calculation Given
     * 	Tests when no payment calculation paramters are given and a single PIDM is requested.
     *	Expected Return: Results seen in the mockSinglePidmResultsNoPaymentCal.
     */
    public function testSinglePidmUniqueID()
    {
        $this->callResourceMockResponse = array();
        //$this->callResourceMockResponse = $this->mockCallToAwardServiceSingleUniqueID();

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSinglePidmUniqueID')));

        try {
            $resp = $this->debtProjection->getDebtProjection();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedInvalidSearchParameterCombo(), $e->getMessage());
        }

    }

    /*
     *	Multiple PIDM and Unique Test with Payment Calculation Given
     * 	Tests when payment calculation paramters are given and a multiple PIDMs and
     *   UniqueID are requested.
     *	Expected Return: Results seen in the mockMultiplePidmResultsNoPaymentCal.
     */
    public function testMultiplePidmUniqueID()
    {
        //$this->callResourceMockResponse = $this->mockCallToAwardServiceMultipleUniqueID();

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsMultiplePidmUniqueID')));

        try {
            $resp = $this->debtProjection->getDebtProjection();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedInvalidSearchParameterCombo(), $e->getMessage());
        }


    }

    /*************************/
    /**Start of Mock Methods**/
    /*************************/

    public function mockAuthorizedUser()
    {
        return true;
    }

    //No Payment Calculation Returns and Parameters Mock Methods

    public function mockOptionsSinglePidmUniqueIDNoPaymentCal()
    {
        $optionsArray = array('pidm' => array('9999999'),
            'uniqueid' => array('testuser')
        );
        return $optionsArray;
    }

    public function mockOptionsMultiplePidmUniqueIDNoPaymentCal()
    {
        $optionsArray = array('pidm' => array('9999999', '1234567', '0000001', '0000000'),
            'uniqueid' => array('testuser', 'testuser1', 'testuser2')
        );
        return $optionsArray;
    }


    public function mockSinglePidmUniqueIDResultsNoPaymentCal()
    {
        $returnArray = array(
            '9999999' => array(
                'perkinsLoanSubTotal' => '2000',
                'universityLoanSubTotal' => '4000',
                'federalLoanSubTotal' => '2',
                'privateLoanSubTotal' => '200',
                'loanTotal' => '6202',
            ),
            'testuser' => array(
                'perkinsLoanSubTotal' => '2000',
                'universityLoanSubTotal' => '4000',
                'federalLoanSubTotal' => '2',
                'privateLoanSubTotal' => '200',
                'loanTotal' => '6202',
            )
        );
        return $returnArray;
    }

    public function mockMultiplePidmUniqueIDResultsNoPaymentCal()
    {
        $returnArray = array(
            '9999999' => array(
                'perkinsLoanSubTotal' => '2000',
                'universityLoanSubTotal' => '4000',
                'federalLoanSubTotal' => '2',
                'privateLoanSubTotal' => '400',
                'loanTotal' => '6402',
            ),
            '1234567' => array(
                'perkinsLoanSubTotal' => '4000',
                'universityLoanSubTotal' => '1000',
                'federalLoanSubTotal' => '0',
                'privateLoanSubTotal' => '0',
                'loanTotal' => '5000',
            ),
            '0000001' => array(
                'perkinsLoanSubTotal' => '0',
                'universityLoanSubTotal' => '2000',
                'federalLoanSubTotal' => '100',
                'privateLoanSubTotal' => '500',
                'loanTotal' => '2600',

            ),
            '0000000' => array(
                'perkinsLoanSubTotal' => '0',
                'universityLoanSubTotal' => '0',
                'federalLoanSubTotal' => '0',
                'privateLoanSubTotal' => '0',
                'loanTotal' => '0',
            ),
            'testuser' => array(
                'perkinsLoanSubTotal' => '2000',
                'universityLoanSubTotal' => '4000',
                'federalLoanSubTotal' => '2',
                'privateLoanSubTotal' => '400',
                'loanTotal' => '6402',
            ),
            'testuser1' => array(
                'perkinsLoanSubTotal' => '4000',
                'universityLoanSubTotal' => '1000',
                'federalLoanSubTotal' => '0',
                'privateLoanSubTotal' => '0',
                'loanTotal' => '5000',
            ),
            'testuser2' => array(
                'perkinsLoanSubTotal' => '0',
                'universityLoanSubTotal' => '2000',
                'federalLoanSubTotal' => '100',
                'privateLoanSubTotal' => '500',
                'loanTotal' => '2600',

            ),
        );
        return $returnArray;
    }

    //Payment Calculation Returns and Parameters Mock Methods

    public function mockOptionsSinglePidmUniqueID()
    {
        $optionsArray = array('pidm' => array('9999999'),
            'uniqueid' => array('testuser'),
            'interestRate' => '1.0',
            'numberOfYears' => 10
        );
        return $optionsArray;
    }

    public function mockOptionsMultiplePidmUniqueID()
    {
        $optionsArray = array('pidm' => array('9999999', '1234567', '0000001', '0000000'),
            'uniqueid' => array('testuser', 'testuser1', 'testuser2'),
            'interestRate' => '1.0',
            'numberOfYears' => 10
        );
        return $optionsArray;
    }

    public function mockSinglePidmUniqueIDResults()
    {
        $returnArray = array(
            '9999999' => array(
                'perkinsLoanSubTotal' => '2000',
                'universityLoanSubTotal' => '4000',
                'federalLoanSubTotal' => '2',
                'privateLoanSubTotal' => '200',
                'loanTotal' => '6202',
                'monthlyPayment' => '54.33',
                'cumulativeTotal' => '6519.60',
                'totalInterest' => '317.60',

            ),
            'testuser' => array(
                'perkinsLoanSubTotal' => '2000',
                'universityLoanSubTotal' => '4000',
                'federalLoanSubTotal' => '2',
                'privateLoanSubTotal' => '200',
                'loanTotal' => '6202',
                'monthlyPayment' => '54.33',
                'cumulativeTotal' => '6519.60',
                'totalInterest' => '317.60',

            )
        );
        return $returnArray;
    }

    public function mockMultiplePidmUniqueIDResults()
    {
        $returnArray = array(
            '9999999' => array(
                'perkinsLoanSubTotal' => '2000',
                'universityLoanSubTotal' => '4000',
                'federalLoanSubTotal' => '2',
                'privateLoanSubTotal' => '400',
                'loanTotal' => '6402',
                'monthlyPayment' => '56.08',
                'cumulativeTotal' => '6729.60',
                'totalInterest' => '327.60',
            ),
            '1234567' => array(
                'perkinsLoanSubTotal' => '4000',
                'universityLoanSubTotal' => '1000',
                'federalLoanSubTotal' => '0',
                'privateLoanSubTotal' => '0',
                'loanTotal' => '5000',
                'monthlyPayment' => '43.80',
                'cumulativeTotal' => '5256.00',
                'totalInterest' => '256.00',
            ),
            '0000001' => array(
                'perkinsLoanSubTotal' => '0',
                'universityLoanSubTotal' => '2000',
                'federalLoanSubTotal' => '100',
                'privateLoanSubTotal' => '500',
                'loanTotal' => '2600',
                'monthlyPayment' => '22.78',
                'cumulativeTotal' => '2733.60',
                'totalInterest' => '133.6',
            ),
            '0000000' => array(
                'perkinsLoanSubTotal' => '0',
                'universityLoanSubTotal' => '0',
                'federalLoanSubTotal' => '0',
                'privateLoanSubTotal' => '0',
                'loanTotal' => '0',
                'monthlyPayment' => '0.00',
                'cumulativeTotal' => '0.00',
                'totalInterest' => '0.00',
            ),
            'testuser' => array(
                'perkinsLoanSubTotal' => '2000',
                'universityLoanSubTotal' => '4000',
                'federalLoanSubTotal' => '2',
                'privateLoanSubTotal' => '400',
                'loanTotal' => '6402',
                'monthlyPayment' => '56.08',
                'cumulativeTotal' => '6729.60',
                'totalInterest' => '327.60',
            ),
            'testuser1' => array(
                'perkinsLoanSubTotal' => '4000',
                'universityLoanSubTotal' => '1000',
                'federalLoanSubTotal' => '0',
                'privateLoanSubTotal' => '0',
                'loanTotal' => '5000',
                'monthlyPayment' => '43.80',
                'cumulativeTotal' => '5256.00',
                'totalInterest' => '256.00',
            ),
            'testuser2' => array(
                'perkinsLoanSubTotal' => '0',
                'universityLoanSubTotal' => '2000',
                'federalLoanSubTotal' => '100',
                'privateLoanSubTotal' => '500',
                'loanTotal' => '2600',
                'monthlyPayment' => '22.78',
                'cumulativeTotal' => '2733.60',
                'totalInterest' => '133.6',
            ),

        );
        return $returnArray;
    }

    //No PIDM or Unique ID Return
    public function mockNoParameters()
    {
        $optionsArray = array();
        return $optionsArray;
    }

    public function mockExpectedNoPidmUniqueIDResult()
    {
        return "Error: At least one pidm, one uniqueid, or one bannerid must be specified.";
    }

    public function mockExpectedInvalidSearchParameterCombo()
    {
        return "Please only search by PIDM, UniqueID or BannerID, not multiples.";
    }

    //Empty PIDM or Unique ID Return
    public function mockEmptyParameters()
    {
        $optionsArray = array('pidm' => array(''),
            'uniqueid' => array(''));
        return $optionsArray;
    }

    public function mockExpectedEmptyPidmUniqueIDResult()
    {
        return "Error: At least one pidm or one uniqueid must be specified.";
    }

    //Mock Award Recoursce calls

    public function callResourceWithName($subject)
    {
        $this->resourceBeingCalledName = $subject;
        return true;
    }

    public function callResourceWithArgs($subject)
    {
        $this->resourceBeingCalledArgs = $subject;
        return true;
    }

    public function callResourceMock()
    {
        $response = new \MiamiOH\RESTng\Util\Response();

        $id = '';
        if (isset($this->resourceBeingCalledArgs['params']['id'])) {
            $id = $this->resourceBeingCalledArgs['params']['id'];
        } elseif (isset($this->resourceBeingCalledArgs['options']['id'])) {
            $id = $this->resourceBeingCalledArgs['options']['id'];
        } elseif (isset($this->resourceBeingCalledArgs['data']['data'][0]['uniqueid'])) {
            $id = $this->resourceBeingCalledArgs['data']['data'][0]['uniqueid'];
        }

        //print "Calling " . $this->resourceBeingCalledName . " ($id)\n";

        if (isset($this->callResourceMockResponse[$this->resourceBeingCalledName])) {
            $mockResponseData = $this->callResourceMockResponse[$this->resourceBeingCalledName];
            $response->setStatus($mockResponseData['status']);
            $response->setPayload($mockResponseData['payload']);
        }

        $this->resourceResponses[$this->resourceBeingCalledName] = array(
            'status' => $response->getStatus(),
            'payload' => $response->getPayload(),
        );

        return $response;

    }

}