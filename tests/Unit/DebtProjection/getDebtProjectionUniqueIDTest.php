<?php

/*
-----------------------------------------------------------
FILE NAME: getDebtProjectionUniqueIDTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION: 
This php class is used to test the GET method of the debtProjection service. Specifically
for testing the UniqueID Parameter

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit
Student/FinancialAid/DebtProjection Service

TABLE USAGE:

Web Service Usage:
	Student/FinancialAid/DebtProjection service (GET)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

02/XX/2016               SCHMIDEE
Description:  Initial Draft
			 
-----------------------------------------------------------
 */
namespace MiamiOH\FinancialAidRest\Tests\Unit\DebtProjection;

use MiamiOH\RESTng\App;

class GetDebtProjectionUniqueIDTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $debtProjection, $queryallRecords, $user, $request, $awardService, $api;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        /*$ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->setMethods(array('getDataSource'))
            ->getMock();*/

        //set up the service with the mocked out resources:
        $this->debtProjection = new \MiamiOH\FinancialAidRest\DebtProjection\Services\DebtProjection();
        $this->debtProjection->setApp($this->api);
        $this->debtProjection->setApiUser($this->user);
        $this->debtProjection->setDatabase($db);
       // $this->debtProjection->setDatasource($ds);
        $this->debtProjection->setRequest($this->request);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /*
     *	Invalid User Test
     * 	Tests Case in which a user is not Authorized to use this service.
     *	Expected Return: 401 Forbidden Error
     *
     public function testInvalidAuthorization() {
         //tell the dbh what to do when the queryall_array method is called.
         $this->user->method('isAuthorized')
         ->will($this->returnCallback(array($this, 'mockNotAuthorizedUser')));
         $resp    = $this->debtProjection->getDebtProjection();

         //get the response and payload from the getSchedule() method.
            $this->assertEquals(MiamiOH\RESTng\App::API_UNAUTHORIZED, $resp->getStatus());
     }
     */

    /*
     *   Empty UniqueIDGiven
     * 	Tests Case in which empty parameteres given at aall.
     *	Expected Return: 400 Error
     */
    public function testEmptyUniqueID()
    {

        $this->callResourceMockResponse = array();
        $this->callResourceMockResponse = $this->mockCallToAwardServiceSingle();

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockEmptyParameters')));

        $this->api->expects($this->exactly(1))->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')), $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));
        try {
            $resp = $this->debtProjection->getDebtProjection();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedEmptyUniqueIDResult(), $e->getMessage());
        }
    }


    /*
	*	Single UniqueID Test with No Payment Calculation Given
	* 	Tests when no payment calculation paramters are given and a single PIDM is requested.
	*	Expected Return: Results seen in the mockSinglePidmResultsNoPaymentCal.
	*/
    public function testSingleUniqueIDNoPaymentCal()
    {
        $this->callResourceMockResponse = array();
        $this->callResourceMockResponse = $this->mockCallToAwardServiceSingle();

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSingleUniqueIDNoPaymentCal')));

        $this->api->expects($this->exactly(1))->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')), $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $resp = $this->debtProjection->getDebtProjection();

        $payload = $resp->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        //echo print_r($payload);
        $this->assertEquals(count($payload), 1);
        $this->assertEquals($payload, $this->mockSingleUniqueIDResultsNoPaymentCal());

    }

    /*
     *	Multiple PIDM Test with No Payment Calculation Given
     * 	Tests when no payment calculation paramters are given and a multiple PIDMs are requested.
     *	Expected Return: Results seen in the mockMultiplePidmResultsNoPaymentCal.
     */
    public function testMultipleUniqueIDNoPaymentCal()
    {
        $this->callResourceMockResponse = $this->mockCallToAwardServiceMultiple();

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsMultipleUniqueIDNoPaymentCal')));

        $this->api->expects($this->exactly(1))->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')), $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $resp = $this->debtProjection->getDebtProjection();
        $payload = $resp->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 4);
        $this->assertEquals($payload, $this->mockMultipleUniqueIDResultsNoPaymentCal());


    }

    /*
     *	Single UniqueID Test with No Payment Calculation Given
     * 	Tests when no payment calculation paramters are given and a single PIDM is requested.
     *	Expected Return: Results seen in the mockSinglePidmResultsNoPaymentCal.
     */
    public function testSingleUniqueID()
    {
        $this->callResourceMockResponse = array();
        $this->callResourceMockResponse = $this->mockCallToAwardServiceSingle();

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSingleUniqueID')));

        $this->api->expects($this->exactly(1))->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')), $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $resp = $this->debtProjection->getDebtProjection();

        $payload = $resp->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 1);
        $this->assertEquals($payload, $this->mockSingleUniqueIDResults());

    }

    /*
     *	Multiple UniqueID Test with Payment Calculation Given
     * 	Tests when payment calculation paramters are given and a multiple PIDMs are requested.
     *	Expected Return: Results seen in the mockMultiplePidmResultsNoPaymentCal.
     */
    public function testMultipleUniqueID()
    {
        $this->callResourceMockResponse = $this->mockCallToAwardServiceMultiple();

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsMultipleUniqueID')));

        $this->api->expects($this->exactly(1))->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')), $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $resp = $this->debtProjection->getDebtProjection();
        $payload = $resp->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 4);
        $this->assertEquals($payload, $this->mockMultipleUniqueIDResults());


    }

    /*************************/
    /**Start of Mock Methods**/
    /*************************/

    public function mockAuthorizedUser()
    {
        return true;
    }

    //No Payment Calculation Returns and Parameters Mock Methods
    public function mockOptionsSingleUniqueIDNoPaymentCal()
    {
        $optionsArray = array('uniqueid' => array('testuser'));
        return $optionsArray;
    }

    public function mockOptionsMultipleUniqueIDNoPaymentCal()
    {
        $optionsArray = array('uniqueid' => array('testuser', 'testuser1', 'testuser2', 'testuser3'));
        return $optionsArray;
    }


    public function mockSingleUniqueIDResultsNoPaymentCal()
    {
        $returnArray = array(
            'testuser' => array(
                'perkinsLoanSubTotal' => '2000',
                'universityLoanSubTotal' => '4000',
                'federalLoanSubTotal' => '2',
                'privateLoanSubTotal' => '200',
                'loanTotal' => '6202',
                'totalFinancialAidByAidYear' => array(
                    '0910' => '3101',
                    '0708' => '1001',
                    '1011' => '100',
                    '1112' => '2000',
                ),
            )
        );
        return $returnArray;
    }

    public function mockMultipleUniqueIDResultsNoPaymentCal()
    {
        $returnArray = array(
            'testuser' => array(
                'perkinsLoanSubTotal' => 2000,
                'universityLoanSubTotal' => 4000,
                'federalLoanSubTotal' => 2,
                'privateLoanSubTotal' => 400,
                'loanTotal' => 6402,
                'totalFinancialAidByAidYear' => array(
                    '0910' => 3101,
                    '0708' => 1001,
                    '1011' => 2200,
                    '1112' => 100
                ),
            ),
            'testuser1' => array(
                'perkinsLoanSubTotal' => 4000,
                'universityLoanSubTotal' => 1000,
                'federalLoanSubTotal' => 0,
                'privateLoanSubTotal' => 0,
                'loanTotal' => 5000,
                'totalFinancialAidByAidYear' => array(
                    '0910' => 3000,
                    '0708' => 2000,
                )
            ),
            'testuser2' => array(
                'perkinsLoanSubTotal' => 0,
                'universityLoanSubTotal' => 2000,
                'federalLoanSubTotal' => 100,
                'privateLoanSubTotal' => 500,
                'loanTotal' => 2600,
                'totalFinancialAidByAidYear' => array(
                    '0405' => 2000,
                    '0708' => 100,
                    '1112' => 500
                )

            ),
            'testuser3' => array(
                'perkinsLoanSubTotal' => 0,
                'universityLoanSubTotal' => 0,
                'federalLoanSubTotal' => 0,
                'privateLoanSubTotal' => 0,
                'loanTotal' => 0,
                'totalFinancialAidByAidYear' => array()
            )
        );
        return $returnArray;
    }


    //Payment Calculation Returns and Parameters Mock Methods
    public function mockOptionsSingleUniqueID()
    {
        $optionsArray = array('uniqueid' => array('testuser'),
            'interestRate' => '1.0',
            'numberOfYears' => 10
        );
        return $optionsArray;
    }

    public function mockOptionsMultipleUniqueID()
    {
        $optionsArray = array('uniqueid' => array('testuser', 'testuser1', 'testuser2', 'testuser2'),
            'interestRate' => '1.0',
            'numberOfYears' => 10
        );
        return $optionsArray;
    }


    public function mockSingleUniqueIDResults()
    {
        $returnArray = array(
            'testuser' => array(
                'perkinsLoanSubTotal' => '2000',
                'universityLoanSubTotal' => '4000',
                'federalLoanSubTotal' => '2',
                'privateLoanSubTotal' => '200',
                'loanTotal' => '6202',
                'monthlyPayment' => '54.33',
                'cumulativeTotal' => '6519.60',
                'totalInterest' => '317.60',
                'totalFinancialAidByAidYear' => array(
                    '0910' => '3101',
                    '0708' => '1001',
                    '1011' => '100',
                    '1112' => '2000',
                )
            )
        );
        return $returnArray;
    }

    public function mockMultipleUniqueIDResults()
    {
        $returnArray = array(
            'testuser' => array(
                'perkinsLoanSubTotal' => 2000,
                'universityLoanSubTotal' => 4000,
                'federalLoanSubTotal' => 2,
                'privateLoanSubTotal' => 400,
                'loanTotal' => 6402,
                'monthlyPayment' => 56.08,
                'cumulativeTotal' => 6729.60,
                'totalInterest' => 327.60,
                'totalFinancialAidByAidYear' => array(
                    '0910' => 3101,
                    '0708' => 1001,
                    '1011' => 2200,
                    '1112' => 100
                ),
            ),
            'testuser1' => array(
                'perkinsLoanSubTotal' => 4000,
                'universityLoanSubTotal' => 1000,
                'federalLoanSubTotal' => 0,
                'privateLoanSubTotal' => 0,
                'loanTotal' => 5000,
                'monthlyPayment' => 43.80,
                'cumulativeTotal' => 5256.00,
                'totalInterest' => 256.00,
                'totalFinancialAidByAidYear' => array(
                    '0910' => 3000,
                    '0708' => 2000,
                )
            ),
            'testuser2' => array(
                'perkinsLoanSubTotal' => 0,
                'universityLoanSubTotal' => 2000,
                'federalLoanSubTotal' => 100,
                'privateLoanSubTotal' => 500,
                'loanTotal' => 2600,
                'monthlyPayment' => 22.78,
                'cumulativeTotal' => 2733.60,
                'totalInterest' => 133.6,
                'totalFinancialAidByAidYear' => array(
                    '0405' => 2000,
                    '0708' => 100,
                    '1112' => 500
                )
            ),
            'testuser3' => array(
                'perkinsLoanSubTotal' => 0,
                'universityLoanSubTotal' => 0,
                'federalLoanSubTotal' => 0,
                'privateLoanSubTotal' => 0,
                'loanTotal' => 0,
                'monthlyPayment' => 0.00,
                'cumulativeTotal' => 0.00,
                'totalInterest' => 0.00,
                'totalFinancialAidByAidYear' => array()
            )
        );
        return $returnArray;
    }


    //No PIDM or Unique ID Return
    public function mockNoParameters()
    {
        $optionsArray = array();
        return $optionsArray;
    }

    //Empty PIDM or Unique ID Return
    public function mockEmptyParameters()
    {
        $optionsArray = array('uniqueid' => array(''));
        return $optionsArray;
    }

    public function mockExpectedEmptyUniqueIDResult()
    {
        return "Error: At least one pidm or one uniqueid must be specified.";
    }

    //Mock Award Recoursce calls
    public function mockCallToAwardServiceSingle()
    {
        $returnArray = array();

        $returnArray = array(
            'student.financialAid.award.get' => array(
                'status' => \MiamiOH\RESTng\App::API_OK,
                'payload' => array(
                    'testuser' => array(
                        '0910' => array(
                            'OCGM' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'OCGM',
                                'fundTitle' => 'MU Coll. Op. Grant Supplement',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Institutional',
                                'fsrcIndicator' => 'I',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '2000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '2000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '2000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '2000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '2000',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '2000',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'TEST' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'TEST',
                                'fundTitle' => 'Test Scholorship',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Other Loan',
                                'fsrcIndicator' => 'O',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '100',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '100',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '100',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '100',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '100',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '100',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '100',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'PERK' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PERK',
                                'fundTitle' => 'Perkins Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'PLUS' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PLUS',
                                'fundTitle' => 'PLUS Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'FEDS' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'FEDS',
                                'fundTitle' => 'FEDS Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            )

                        ),
                        '0708' => array(
                            'PELL' => array(
                                'aidYearCode' => '0708',
                                'fundCode' => 'PELL',
                                'fundTitle' => 'Federal Pell Grant',
                                'fsrcCode' => 'FDRL',
                                'fsrcDescription' => 'Federal',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'GRNT',
                                'fundTypeDescription' => 'Grant',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'S',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '2496',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '2496',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'PERK' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PERK',
                                'fundTitle' => 'Perkin\' Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2496',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'OCGM' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'OCGM',
                                'fundTitle' => 'MU Coll. Op. Grant Supplement',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Institutional',
                                'fsrcIndicator' => 'I',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '2000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '2000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '2000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '2000',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '2000',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'TEST' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'TEST',
                                'fundTitle' => 'Test Scholorship',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Other Loan',
                                'fsrcIndicator' => 'O',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '100',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '100',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '100',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '100',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '100',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '0',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '100',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '100',
                                'cancelDate' => '27-MAY-09',
                            ),
                        ),
                        '1011' => array(
                            'PERK' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PERK',
                                'fundTitle' => 'Perkin\' Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2496',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'OCGM' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'OCGM',
                                'fundTitle' => 'MU Coll. Op. Grant Supplement',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Institutional',
                                'fsrcIndicator' => 'I',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '2000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '2000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '2000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '2000',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '2000',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'FEDS' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'FEDS',
                                'fundTitle' => 'FEDS Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'TEST' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'TEST',
                                'fundTitle' => 'Test Scholorship',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Other Loan',
                                'fsrcIndicator' => 'O',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '100',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '100',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '100',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '100',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '100',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '100',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '100',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '100',
                                'cancelDate' => '27-MAY-09',
                            ),
                        ),
                        '1112' => array(
                            'PERK' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PERK',
                                'fundTitle' => 'Perkins Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '0',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '0',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'OCGM' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'OCGM',
                                'fundTitle' => 'MU Coll. Op. Grant Supplement',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Institutional',
                                'fsrcIndicator' => 'I',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '2000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '2000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '2000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '0',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '2000',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '2000',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'FEDS' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'FEDS',
                                'fundTitle' => 'FEDS Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '0',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '0',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'TEST' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'TEST',
                                'fundTitle' => 'Test Scholorship',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Other Loan',
                                'fsrcIndicator' => 'O',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '100',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '100',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '100',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '100',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '100',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '0',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '100',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '100',
                                'cancelDate' => '27-MAY-09',
                            ),
                        ),
                    ),
                ),));

        return $returnArray;
    }

    public function mockCallToAwardServiceMultiple()
    {
        $returnArray = array();
        $returnArray = array(
            'student.financialAid.award.get' => array(
                'status' => \MiamiOH\RESTng\App::API_OK,
                'payload' => array(
                    'testuser' => array(
                        '0910' => array(
                            'OCGM' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'OCGM',
                                'fundTitle' => 'MU Coll. Op. Grant Supplement',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Institutional',
                                'fsrcIndicator' => 'I',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '2000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '2000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '2000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '2000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '2000',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '2000',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'TEST' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'TEST',
                                'fundTitle' => 'Test Scholorship',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Other Loan',
                                'fsrcIndicator' => 'O',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '100',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '100',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '100',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '100',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '100',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '100',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '100',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '100',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'PERK' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PERK',
                                'fundTitle' => 'Perkins Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'PLUS' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PLUS',
                                'fundTitle' => 'PLUS Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2496',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'FEDS' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'FEDS',
                                'fundTitle' => 'FEDS Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            )

                        ),
                        '0708' => array(
                            'PELL' => array(
                                'aidYearCode' => '0708',
                                'fundCode' => 'PELL',
                                'fundTitle' => 'Federal Pell Grant',
                                'fsrcCode' => 'FDRL',
                                'fsrcDescription' => 'Federal',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'GRNT',
                                'fundTypeDescription' => 'Grant',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'S',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '2496',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '2496',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'PERK' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PERK',
                                'fundTitle' => 'Perkin\' Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'OCGM' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'OCGM',
                                'fundTitle' => 'MU Coll. Op. Grant Supplement',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Institutional',
                                'fsrcIndicator' => 'I',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '2000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '2000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '2000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '2000',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '2000',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'TEST' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'TEST',
                                'fundTitle' => 'Test Scholorship',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Other Loan',
                                'fsrcIndicator' => 'O',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '0',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '100',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '0',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '100',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '0',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '100',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '100',
                                'cancelDate' => '27-MAY-09',
                            ),
                        ),
                        '1011' => array(
                            'PERK' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PERK',
                                'fundTitle' => 'Perkin\' Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2496',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'OCGM' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'OCGM',
                                'fundTitle' => 'MU Coll. Op. Grant Supplement',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Institutional',
                                'fsrcIndicator' => 'I',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '2000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '2000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '2000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '2000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '2000',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '2000',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'FEDS' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'FEDS',
                                'fundTitle' => 'FEDS Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'TEST' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'TEST',
                                'fundTitle' => 'Test Scholorship',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Other Loan',
                                'fsrcIndicator' => 'O',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '0',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '100',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '100',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '100',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '200',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '100',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '100',
                                'cancelDate' => '27-MAY-09',
                            ),
                        ),
                        '1112' => array(
                            'PERK' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PERK',
                                'fundTitle' => 'Perkins Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '0',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '0',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'OCGM' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'OCGM',
                                'fundTitle' => 'MU Coll. Op. Grant Supplement',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Institutional',
                                'fsrcIndicator' => 'I',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '0',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '2000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '2000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '0',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '2000',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '2000',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'FEDS' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'FEDS',
                                'fundTitle' => 'FEDS Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '0',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '0',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'TEST' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'TEST',
                                'fundTitle' => 'Test Scholorship',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Other Loan',
                                'fsrcIndicator' => 'O',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '100',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '0',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '0',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '100',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '100',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '100',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '100',
                                'cancelDate' => '27-MAY-09',
                            ),
                        ),
                    ),
                    'testuser1' => array(
                        '0910' => array(
                            'PLUS' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PLUS',
                                'fundTitle' => 'Plus loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2496',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'PERK' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PERK',
                                'fundTitle' => 'Perkins Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '3000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2496',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '3000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '3000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            )
                        ),
                        '0708' => array(
                            'OCGM' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'OCGM',
                                'fundTitle' => 'MU Coll. Op. Grant Supplement',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Institutional',
                                'fsrcIndicator' => 'I',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '2000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '1000',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '1000',
                                'cancelDate' => '27-MAY-09',
                            ),
                            'PERK' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PERK',
                                'fundTitle' => 'Perkins Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2496',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '1000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '1000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            ),
                        ),
                        '1011' => array(
                            'PERK' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PERK',
                                'fundTitle' => 'Perkins Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '1000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2496',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '2000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            )
                        ),
                        '1112' => array(
                            'PERK' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'PERK',
                                'fundTitle' => 'Perkins Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '0',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '1000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '1000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '2000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '1000',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '0',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            )
                        ),
                    ),
                    'testuser2' => array(
                        '0405' => array(
                            'OCGM' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'OCGM',
                                'fundTitle' => 'MU Coll. Op. Grant Supplement',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Institutional',
                                'fsrcIndicator' => 'I',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '2000',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '2000',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '2000',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '2000',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '2000',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '2000',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '2000',
                                'cancelDate' => '27-MAY-09',
                            ),
                        ),
                        '0708' => array(
                            'PELL' => array(
                                'aidYearCode' => '0708',
                                'fundCode' => 'PELL',
                                'fundTitle' => 'Federal Pell Grant',
                                'fsrcCode' => 'FDRL',
                                'fsrcDescription' => 'Federal',
                                'fsrcIndicator' => 'F',
                                'fundTypeCode' => 'GRNT',
                                'fundTypeDescription' => 'Grant',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'S',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '100',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '2496',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '100',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '100',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '2496',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '100',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '100',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '100',
                                'cancelDate' => '27-MAY-09',
                            ),
                        ),
                        '1011' => array(
                            'TEST' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'TEST',
                                'fundTitle' => 'Test Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Other Loan',
                                'fsrcIndicator' => 'O',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '0',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '0',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '0',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '500',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '0',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '0',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '0',
                                'cancelDate' => '27-MAY-09',
                            )
                        ),
                        '1112' => array(
                            'TEST' => array(
                                'aidYearCode' => '0910',
                                'fundCode' => 'TEST',
                                'fundTitle' => 'Perkins Loan',
                                'fsrcCode' => 'INST',
                                'fsrcDescription' => 'Federal Loan',
                                'fsrcIndicator' => 'O',
                                'fundTypeCode' => 'SCHN',
                                'fundTypeDescription' => 'Need Based Scholarship',
                                'awstCode' => 'ACPT',
                                'awstDescription' => 'Accepted',
                                'awstDate' => '27-MAY-09',
                                'systemIndicator' => 'M',
                                'activityDate' => '01-JAN-10',
                                'lockIndicator' => 'N',
                                'offerExpirationDate' => '27-MAY-09',
                                'acceptAmount' => '500',
                                'acceptDate' => '27-MAY-09',
                                'authorizeAmount' => '500',
                                'authorizeDate' => '27-MAY-09',
                                'memoAmount' => '500',
                                'memoDate' => '27-MAY-09',
                                'paidAmount' => '0',
                                'paidDate' => '27-MAY-09',
                                'originalOfferAmount' => '500',
                                'originalOfferDate' => '27-MAY-09',
                                'offerAmount' => '500',
                                'offerDate' => '27-MAY-09',
                                'declineAmount' => '500',
                                'declineDate' => '27-MAY-09',
                                'cancelAmount' => '500',
                                'cancelDate' => '27-MAY-09',
                            )
                        ),
                    ),
                    'testuser3' => array(),
                ),)
        );
        return $returnArray;
    }

    public function callResourceWithName($subject)
    {
        $this->resourceBeingCalledName = $subject;
        return true;
    }

    public function callResourceWithArgs($subject)
    {
        $this->resourceBeingCalledArgs = $subject;
        return true;
    }

    public function callResourceMock()
    {
        $response = new \MiamiOH\RESTng\Util\Response();

        $id = '';
        if (isset($this->resourceBeingCalledArgs['params']['id'])) {
            $id = $this->resourceBeingCalledArgs['params']['id'];
        } elseif (isset($this->resourceBeingCalledArgs['options']['id'])) {
            $id = $this->resourceBeingCalledArgs['options']['id'];
        } elseif (isset($this->resourceBeingCalledArgs['data']['data'][0]['uniqueid'])) {
            $id = $this->resourceBeingCalledArgs['data']['data'][0]['uniqueid'];
        }

        //print "Calling " . $this->resourceBeingCalledName . " ($id)\n";

        if (isset($this->callResourceMockResponse[$this->resourceBeingCalledName])) {
            $mockResponseData = $this->callResourceMockResponse[$this->resourceBeingCalledName];
            $response->setStatus($mockResponseData['status']);
            $response->setPayload($mockResponseData['payload']);
        }

        $this->resourceResponses[$this->resourceBeingCalledName] = array(
            'status' => $response->getStatus(),
            'payload' => $response->getPayload(),
        );

        return $response;

    }

}