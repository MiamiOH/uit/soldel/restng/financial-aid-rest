<?php

return [
    'resources' => [
        'student' => [
            MiamiOH\FinancialAidRest\Award\Resources\AwardResourceProvider::class,
            MiamiOH\FinancialAidRest\DebtProjection\Resources\DebtProjectionResourceProvider::class,
            MiamiOH\FinancialAidRest\Refund\Resources\RefundResourceProvider::class,
        ],
    ]
];